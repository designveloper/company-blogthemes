<?php
/*=============================
	DEFAULT BLOG LISTING PAGE
=============================*/
get_header();
get_template_part( 'includes/title' );
global $wp_query;

$cur_page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; //get curent page
$page_links_total =  $wp_query->max_num_pages;
$page_links = paginate_links( 
	array(
		'base' => esc_url( add_query_arg( 'paged', '%#%' ) ),
		'prev_next' => true,
		'end_size' => 2,
		'mid_size' => 2,
		'total' => $page_links_total,
		'current' => $cur_page,	
		'prev_next' => false,
		'type' => 'array'
	)
);	
$pagination = linetime_format_pagination( $page_links );


?>
<section>
	<div class="container">
		<?php if ( is_category() || is_tag() || is_author() || is_archive() || is_search() ): ?>
			<h4 class="filter-title">
				<?php 
					if ( is_category() ){
						echo __('Category: ', 'linetime');
						single_cat_title();
					}
					else if( is_tag() ){
						echo __('Search by tag: ', 'linetime'). get_query_var('tag'); 
					}
					else if( is_author() ){
						echo __('Posts written by ', 'linetime'). get_the_author_meta( 'display_name' ); 
					}
					else if( is_archive() ){
						echo __('Archive for ', 'linetime'). single_month_title(' ',false); 
					}
					else if( is_search() ){ 
						echo __('Search results for: ', 'linetime').' '. get_search_query();
					}
				?>
			</h4>
		<?php endif; ?>	
		<?php 
		if( have_posts() ){
			echo '<div class="timeline">';
			$counter = 0;
			while( have_posts() ){
				the_post();
				$class = 'right-line';
				if( $counter % 2 == 0 ){
					$class = "left-line";
				}
				$counter++;
				$class .= linetime_has_media() ? ' has-media' : '';
				$image_size = 'box-item';
				$post_format = get_post_format();
				?>
					<div <?php post_class( $class.' blog-item clearfix' ) ?>>
						<div class="blog-item-wrapper animation">
							<div class="blog-media">
								<?php include( locate_template( 'media/media'.( !empty( $post_format ) ? '-'.$post_format : '' ).'.php' ) ); ?>
							</div>
							<div class="blog-item-content">

								<div class="category-list">
									<?php echo linetime_the_category() ?>
								</div>
								
								<?php
								$extra_class = '';
								$words = explode( " ", get_the_title() );
								foreach( $words as $word ){
									if( strlen( $word ) > 25 ){
										$extra_class = 'break-word';
									}
								}
								?>
								<div class="text-center">
									<?php
									if( is_sticky() ){
										?>
										<div class="sticky-icon">
											<i class="fa fa-thumb-tack"></i>
										</div>
										<?php
									}
									?>
									<a href="<?php the_permalink() ?>" class="blog-title <?php echo esc_attr( $extra_class ); ?>">
										<h4><?php the_title(); ?></h4>
									</a>
								</div>

								<?php the_excerpt(); ?>

								<div class="text-center">
									<a href="<?php the_permalink() ?>" class="read-more animation">
										<?php _e( 'CONTINUE READING', 'linetime' ); ?>
										<i class="fa fa-long-arrow-right animation"></i>
									</a>
								</div>

								<ul class="list unstyled list-inline blog-meta clearfix">
									<li>
										<i class="fa fa-user"></i>
										<?php the_author_posts_link(); ?>
									</li>
									<li>
										<i class="fa fa-clock-o"></i>
										<?php the_time( 'F j, Y' ); ?>
									</li>
									<li>
										<i class="fa fa-comment-o"></i>
										<?php comments_number( __( '0 Comments', 'linetime' ), __( '1 Comment', 'linetime' ), __( '% Comments', 'linetime' ) ); ?>
									</li>
								</ul>							
							</div>
						</div>
					</div>
				<?php
			}
			echo '<div class="clearfix"></div>';
			if( !empty( $pagination ) ){
				$next = get_next_posts_link();
				$temp = explode( "href=\"", $next );
				$temp2 = explode( "\"", $temp[1] );
				?> 
				<a href="javascript:;" class="load-more" data-next_link="<?php echo esc_url( $temp2[0] ); ?>" title="<?php _e( 'Load More', 'linetime' ) ?>">
					<i class="fa fa-angle-double-down"></i>
				</a>
				<?php
			}
			echo '</div>';
		}
		else{
			?>
			<div class="blog-item-content margin-bottom text-center">
				<h3><?php _e( 'No posts found', 'linetime' ) ?></h3>
				<div class="post-content">
					<p><?php _e( 'We could not find any post which matches your search criteria. Use form bellow to try again.', 'linetime' ) ?></p>
					<?php get_search_form(); ?>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</section>

<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>