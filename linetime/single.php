<?php
/*=============================
	DEFAULT SINGLE
=============================*/
get_header();
the_post();

$post_pages = wp_link_pages( 
	array(
		'before' => '',
		'after' => '',
		'link_before'      => '<span>',
		'link_after'       => '</span>',
		'next_or_number'   => 'number',
		'nextpagelink'     => __( '&raquo;', 'linetime' ),
		'previouspagelink' => __( '&laquo;', 'linetime' ),			
		'separator'        => ' ',
		'echo'			   => 0
	) 
);

$image_size = 'post-thumbnail';
$post_format = get_post_format();

?>
<section class="single-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<?php if( linetime_has_media() ): ?>
					<div class="blog-media">
						<?php include( locate_template( 'media/media'.( !empty( $post_format ) ? '-'.$post_format : '' ).'.php' ) ); ?>
					</div>
				<?php endif; ?>
				<div class="blog-item-content margin-bottom">

					<div class="category-list">
						<?php echo linetime_the_category() ?>
					</div>

					<h2 class="post-title"><?php the_title() ?></h2>
					<p class="post-subtitle">
						<?php _e( 'By ', 'linetime' ) ?>
						<?php the_author_posts_link(); ?>
						<?php _e( ' on ', 'linetime' ) ?>
						<?php the_time( 'F j, Y' ); ?>
					</p>

					<div class="clearfix">
						<?php the_content() ?>
					</div>

					<?php get_template_part( 'includes/share' ) ?>
				</div>

				<?php if( !empty( $post_pages ) ): ?>	
					<div class="pagination">
						<?php echo linetime_link_pages( $post_pages ); ?>
					</div>
				<?php endif; ?>				

				<div class="blog-item-content">
					<?php 
					$tags = linetime_the_tags();
					if( !empty( $tags ) ):
					?>
						<div class="post-tags clearfix">
							<?php echo $tags; ?>
						</div>
					<?php
					endif;
					?>
				</div>

				<?php
				$prev_post = get_previous_post();
				$next_post = get_next_post();
				if( !empty( $prev_post ) || !empty( $next_post ) ):
				?>
					<div class="row next-prev margin-bottom">
						<div class="col-sm-5 prev-post">
							<?php
							if( !empty( $prev_post ) ){
								?>
								<p>
									<a href="<?php echo get_permalink( $prev_post->ID ) ?>">
										<i class="fa fa-long-arrow-left"></i>
										<?php echo $prev_post->post_title ?>
									</a>
								</p>
								<?php
							}
							?>
						</div>
						<div class="col-sm-2">
							<div class="text-center">
								<a href="<?php echo esc_url( home_url() ) ?>">
									<i class="fa fa-th-large"></i>
								</a>
							</div>
						</div>
						<div class="col-sm-5 text-right next-post">
							<?php
							if( !empty( $next_post ) ){
								$has_media = has_post_thumbnail( $next_post->ID );
								?>
								<p>
									<a href="<?php echo get_permalink( $next_post->ID ) ?>">
										<?php echo $next_post->post_title ?>
										<i class="fa fa-long-arrow-right"></i>
									</a>
								</p>
								<?php
							}
							?>
						</div>
					</div>
				<?php endif; ?>

				<?php
				$tags = get_the_tags();
				$tags_array = array();
				if( !empty( $tags ) ){
					foreach( $tags as $tag ){
						$tags_array[] = $tag->term_id;
					}
				}
				$related = new WP_Query(array(
					'post_type' => 'post',
					'posts_per_page' => '10',
					'post_status' => 'publish',
					'post__not_in' => array( get_the_ID() ),
					'tag__in' => $tags_array
				));
				if( $related->have_posts() ):
				?>

					<div class="blog-item-content margin-bottom">
						<div class="widget-title-wrap">
							<p class="widget-title">
								<?php _e( 'You may also like', 'linetime' ); ?>
							</p>
						</div>				
						<div class="related-posts">
							<?php while( $related->have_posts() ): ?>
								<?php $related->the_post(); ?>
								<div class="related-item">
									<?php if( has_post_thumbnail() ): ?>
										<div class="blog-media">
											<?php the_post_thumbnail( 'box-item-small' ); ?>
										</div>
										<?php
										$extra_class = '';
										$words = explode( " ", get_the_title() );
										foreach( $words as $word ){
											if( strlen( $word ) > 25 ){
												$extra_class = 'break-word';
											}
										}
										?>										
										<div class="text-center">
											<a href="<?php the_permalink() ?>" class="blog-title <?php echo esc_attr( $extra_class ); ?>">
												<h5><?php the_title(); ?></h5>
											</a>
										</div>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
					</div>

				<?php endif; wp_reset_postdata(); ?>

				<?php comments_template( '', true ) ?>

			</div>
			<div class="col-md-3">
				<div class="widget blog-item-content about-author">
					<div class="widget-title-wrap">
						<p class="widget-title">
							<?php _e( 'About Author', 'linetime' ); ?>
						</p>
					</div>
					<div class="author-info clearfix">
						<?php
						$avatar_url = linetime_get_avatar_url( get_avatar( get_the_author_meta('ID'), 300 ) );
						if( !empty( $avatar_url ) ):
						?>
							<img src="<?php echo esc_url( $avatar_url ) ?>" class="img-responsive" alt="author"/>
						<?php
						endif;
						?>
						<p><?php echo get_the_author_meta( 'description' ); ?></p>
					</div>
				</div>		
				<?php get_sidebar( 'blog' ) ?>
			</div>
		</div>
	</div>	
</section>

<?php get_footer(); ?>