jQuery(document).ready(function($){
	"use strict";
	
	/* TO TOP */
	$('.to_top').click(function(e){
		e.preventDefault();
		$("html, body").stop().animate(
			{
				scrollTop: 0
			}, 
			{
				duration: 1200
			}
		);		
	});	
	

	/* PRELODER */
	$(window).load(function(){
		$('.preloader').fadeOut(200);
	});

	$(document).on( 'click', 'a', function(){
		if( $(this).hasClass('.no-loader') && $(this).attr('href')[0] != '#' && !$(this).hasClass( 'gallery-item' ) && !$(this).hasClass( 'share' ) && $(this).attr('href') !== 'javascript:;' ){
			$('.preloader').fadeIn(100);
		}
	});	

	/* STICKY NAVIGATION */

	var $navigation_bar = $('.navigation-bar');
	var $sticky_nav = $navigation_bar.clone().addClass('sticky_nav');
	$('body').append( $sticky_nav );
	function sticky_nav(){
		var $admin = $('#wpadminbar');
		if( $admin.length > 0 && $admin.css( 'position' ) == 'fixed' ){
			$sticky_nav.css( 'top', $admin.height() );
		}
		else{
			$sticky_nav.css( 'top', '0' );
		}
	}

	$(window).on('scroll', function(){
		if( $(window).scrollTop() >= $navigation_bar.position().top + $navigation_bar.height() && $(window).width() > 769 ){
			$sticky_nav.slideDown();
		}
		else{
			$sticky_nav.slideUp();
		}
	});	
	sticky_nav();

	$(window).resize(function(){
		sticky_nav();
	});

	/* NAVIGATION */
	function handle_navigation(){
		if ($(window).width() >= 767) {
			$('ul.nav li.dropdown, ul.nav li.dropdown-submenu').hover(function () {
				$(this).addClass('open').find(' > .dropdown-menu').stop(true, true).hide().slideDown(200);
			}, function () {
				$(this).removeClass('open').find(' > .dropdown-menu').stop(true, true).show().slideUp(200);
	
			});
		}
		else{
			$('ul.nav li.dropdown, ul.nav li.dropdown-submenu').unbind('mouseenter mouseleave');
		}
	}
	handle_navigation();
	
	$(window).resize(function(){
		setTimeout(function(){
			handle_navigation();
		}, 200);
	});		

	/* SUBMIT FORMS */
	$('.submit_form').click(function(){
		$(this).parents('form').submit();
	});
	
	
	/* SUBSCRIBE */
	$('.subscribe').click( function(e){
		e.preventDefault();
		var $this = $(this);
		var $parent = $this.parents('.subscribe-form');		
		
		$.ajax({
			url: ajaxurl,
			method: "POST",
			data: {
				action: 'subscribe',
				email: $parent.find( '.email' ).val()
			},
			dataType: "JSON",
			success: function( response ){
				if( !response.error ){
					$parent.find('.sub_result').html( '<div class="alert alert-success" role="alert"><span class="fa fa-check-circle"></span> '+response.success+'</div>' );
				}
				else{
					$parent.find( '.sub_result' ).html( '<div class="alert alert-danger" role="alert"><span class="fa fa-times-circle"></span> '+response.error+'</div>' );
				}
			}
		})
	} );
		
	/* contact script */
	$('.send-contact').click(function(e){
		e.preventDefault();
		
		$.ajax({
			url: ajaxurl,
			method: "POST",
			data: {
				action: 'contact',
				name: $('.name').val(),
				email: $('.email').val(),
				subject: $('.subject').val(),
				message: $('.message').val()
			},
			dataType: "JSON",
			success: function( response ){
				if( !response.error ){
					$('.send_result').html( '<div class="alert alert-success" role="alert"><span class="fa fa-check-circle"></span> '+response.success+'</div>' );
				}
				else{
					$('.send_result').html( '<div class="alert alert-danger" role="alert"><span class="fa fa-times-circle"></span> '+response.error+'</div>' );
				}
			}
		})
	});
	
	/* MAGNIFIC POPUP FOR THE GALLERY */
	$('.gallery').each(function(){
		var $this = $(this);
		$this.magnificPopup({
			type:'image',
			delegate: 'a',
			gallery:{enabled:true},
		});
	});


	/* RESPONSIVE SLIDES FOR THE GALLERY POST TYPE */
	function start_slides(){
		$('.post-slider').responsiveSlides({
			speed: 800,
			auto: false,
			pager: false,
			nav: true,
			prevText: '<i class="fa fa-angle-left"></i>',
			nextText: '<i class="fa fa-angle-right"></i>',
		});	
	}
	start_slides()

	/* PAGINATION */
	$('.load-more').click(function(e){
		e.preventDefault();
		var $this = $(this);
		var next_link = $this.data('next_link');
		$this.find('i').attr( 'class', 'fa fa-spin fa-spinner' );
		var last_class = $('.timeline .blog-item:eq('+( $('.blog-item').length - 1 )+')').attr('class');
		var counter = 2;		
		if( last_class.indexOf( 'left-line' ) !== -1 ){
			counter = 1;
		}
		$.ajax({
			url: next_link,
			success: function( response ){
				$(response).find( '.timeline .blog-item' ).each(function(){
					var $$this = $(this);
					var $el = $$this.clone();

					if( counter % 2 == 0 ){
						$el.removeClass('right-line').addClass('left-line');
					}
					else{
						$el.removeClass('left-line').addClass('right-line');
					}

					$this.prev().before( $el );

					counter++;
				});
				
				var $link = $(response).find( '.load-more' ).attr( 'data-next_link' );
				
				if( $link != "" ){
					$this.data( 'next_link', $link );
				}
				else{
					$this.remove();
				}
				start_slides();
			},
			complete: function(){
				$this.find('i').attr( 'class', 'fa fa-angle-double-down' );
			}
		})
	});	

	/* RELATED POSTS */
	$('.related-posts').owlCarousel({
		items: 3,
		responsiveClass: true,
		margin: 20,
		responsive: {
			0: {
				items: 1
			},
			400: {
				items: 2
			},
			768: {
				items: 3
			}
		},
		nav: true,
		navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		dots: false
	});

	/* SEARCH OVERLAY */
	$('.search-overlay-trigger').click(function(){
		$('.search-overlay').fadeIn();
	});

	$('.search-overlay-bg').click(function(e){
		$('.search-overlay').fadeOut();
	});

	/* FEATURED SLIDER */
	$('.featured-slider').owlCarousel({
		items: 1,
		responsiveClass: true,
		margin: 20,
		nav: true,
		navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		dots: false,
		onInitialize: function(){
			calc_caption();
		},
		onResized: function(){
			calc_caption();
		}
	});

	function calc_caption(){
		$('.featured-slider .featured-item').each(function(){
			var $this = $(this);
			var height = $this.height();
			var width = $this.width();

			var $caption = $this.find( '.featured-caption' );
			var captionHeight = $caption.height();
			var captionWidth = $caption.outerWidth();

			$caption.css( 'top', ( height/2 - captionHeight/2 ) + 25 );
			$caption.css( 'left', width/2 - captionWidth/2 );
		});
	}

	/* INSTAGRAM */
	if( $('.instagram-feed').length > 0 ){
		$('.instagram-feed').on('didLoadInstagram', function(event, response) {

			var data = response.data;

			for( var key in data ) {
				var image_src = data[key]['images']['standard_resolution']['url'];
				var image_caption = '';
				if( data[key]['caption']['text'] ){
					image_caption = data[key]['caption']['text'];
				}
				var image_link = data[key]['link'];
	 			var width = 100 / instagram.count;
				var output = '<div style="width: '+width+'%" class="instagram-image"><a href="'+image_link+'" target="_blank" class="no-loader"><img src="'+image_src+'" alt="'+image_caption+'" /></a></div>';

				$('.instagram-feed').append(output);
			}

		});

		$('.instagram-feed').instagram({
			clientId: '972fed4ff0d5444aa21645789adb0eb0',
			count: instagram.count,
			userId: instagram.userID,
		});	
	}
});