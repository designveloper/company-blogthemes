<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="keywords" content="<?php echo esc_attr( linetime_get_option( 'seo_keywords' ) ) ?>"/>
    <meta name="description" content="<?php echo esc_attr( linetime_get_option( 'seo_description' ) ) ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Favicon -->
	<?php 
		$favicon = linetime_get_option( 'site_favicon' );
		if( !empty( $favicon['url'] ) ):
	?>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( $favicon['url'] ); ?>">
	<?php
		endif;
	?>
	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="preloader">
	<div class="loader-inner ball-triangle-path">
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>
<!-- ==================================================================================================================================
TOP BAR
======================================================================================================================================= -->

<section class="navigation-bar">
	<div class="container">
		<div id="navigation">
			<div class="clearfix">
				<div class="pull-left logo-small-screen">
					<button class="navbar-toggle button-white menu" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only"><?php _e( 'Toggle navigation', 'linetime' ) ?></span>
						<i class="fa fa-bars"></i>
					</button>
				</div>	
				<div class="pull-right">
					<a href="javascript:;" class="search-overlay-trigger small-screens">
						<i class="fa fa-search"></i>
					</a>
				</div>
			</div>
			<div class="navbar navbar-default" role="navigation">
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<?php
						if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ 'main-navigation' ] ) ) {
							wp_nav_menu( array(
								'theme_location'  	=> 'main-navigation',
								'container'			=> false,
								'echo'          	=> true,
								'items_wrap'        => '%3$s',
								'walker' 			=> new linetime_walker
							) );
						}
						?>
						<li class="pull-right">
							<a href="javascript:;" class="search-overlay-trigger">
								<i class="fa fa-search"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
$logo = linetime_get_option( 'site_logo' );
if( !empty( $logo['url'] ) ): ?>
<section class="logo">
	<div class="container">
		<a href="<?php echo esc_url( home_url( '/' ) ) ?>">
			<img src="<?php echo esc_url( $logo['url'] ) ?>" alt="" width="<?php echo esc_attr( $logo['width'] ) ?>" height="<?php echo esc_attr( $logo['height'] ) ?>"/>
		</a>
	</div>
</section>
<?php endif; ?>

<div class="search-overlay">
	<div class="search-overlay-bg"> </div>
	<div class="search-box">
		<div class="widget-title-wrap">
			<p class="widget-title">
				<?php _e( 'FILTER POSTS', 'linetime' ); ?>
			</p>
		</div>
		<?php echo get_search_form(); ?>
	</div>
</div>

<?php
$featured = new WP_Query(array(
	'posts_per_page' => '-1',
	'post_status' => 'publish',
	'post_type' => 'post',
	'ignore_sticky_posts' => true,
	'meta_query' => array(
		array(
			'key' => 'featured_post',
			'value' => 'yes'
		)
	)
));

if( $featured->have_posts() && is_home() ):?>
<section>
	<div class="container">
		<div class="featured-slider">
			<?php
			while( $featured->have_posts() ){
				$featured->the_post();
				$style = '';
				if( has_post_thumbnail() ){
					$image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumbnail' );
					$style = 'background-image: url('.esc_url( $image_data[0] ).')';
				}
				?>
				<div class="featured-item" style="<?php echo esc_attr( $style ) ?>">
					<div class="featured-caption">
						<div class="widget-title-wrap">
							<p class="widget-title">
								<?php echo linetime_the_category() ?>
							</p>
						</div>
						<div class="text-center">
							<a href="<?php the_permalink() ?>" class="featured-title">
								<h4><?php the_title() ?></h4>
							</a>						
							<a href="<?php the_permalink() ?>" class="read-more animation">
								<?php _e( 'CONTINUE READING', 'linetime' ); ?>
								<i class="fa fa-long-arrow-right animation"></i>
							</a>
						</div>						
					</div>
				</div>
				<?php
			}
			?>
		</div>
	</div>
</section>
<?php endif; wp_reset_postdata(); ?>