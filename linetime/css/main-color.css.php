<?php

	/* NAVIGATION */
	$navigation_bg_color = linetime_get_option( 'navigation_bg_color' );
	$navigation_font_color = linetime_get_option( 'navigation_font_color' );
	$navigation_active_color = linetime_get_option( 'navigation_active_color' );
	$subnavigation_bg_color = linetime_get_option( 'subnavigation_bg_color' );
	$subnavigation_font_color = linetime_get_option( 'subnavigation_font_color' );
	$subnavigation_active_color = linetime_get_option( 'subnavigation_active_color' );
	$subnavigation_border_color = linetime_get_option( 'subnavigation_border_color' );
	$navigation_font = linetime_get_option( 'navigation_font' );
	$navigation_font_size = linetime_get_option( 'navigation_font_size' );

	/* TEXT */
	$text_font = linetime_get_option( 'text_font' );
	$text_font_size = linetime_get_option( 'text_font_size' );
	$text_line_height = linetime_get_option( 'text_line_height' );

	$title_font = linetime_get_option( 'title_font' );
	$h1_font_size = linetime_get_option( 'h1_font_size' );
	$h1_line_height = linetime_get_option( 'h1_line_height' );
	$h2_font_size = linetime_get_option( 'h2_font_size' );
	$h2_line_height = linetime_get_option( 'h2_line_height' );
	$h3_font_size = linetime_get_option( 'h3_font_size' );
	$h3_line_height = linetime_get_option( 'h3_line_height' );
	$h4_font_size = linetime_get_option( 'h4_font_size' );
	$h4_line_height = linetime_get_option( 'h4_line_height' );
	$h5_font_size = linetime_get_option( 'h5_font_size' );
	$h5_line_height = linetime_get_option( 'h5_line_height' );
	$h6_font_size = linetime_get_option( 'h6_font_size' );
	$h6_line_height = linetime_get_option( 'h6_line_height' );

	/* PAGE TITLES */
	$page_title_bg_color = linetime_get_option( 'page_title_bg_color' );
	$page_title_font_color = linetime_get_option( 'page_title_font_color' );

	/* MAIN BUTTONS */
	$buttons_bg_color = linetime_get_option( 'buttons_bg_color' );
	$buttons_border_color = linetime_get_option( 'buttons_border_color' );
	$buttons_font_color = linetime_get_option( 'buttons_font_color' );
	$buttons_bg_color_hvr = linetime_get_option( 'buttons_bg_color_hvr' );
	$buttons_border_color_hvr = linetime_get_option( 'buttons_border_color_hvr' );
	$buttons_font_color_hvr = linetime_get_option( 'buttons_font_color_hvr' );

	/* TYPOGRAPHY COLORS */
	$title_color = linetime_get_option( 'title_color' );
	$link_color = linetime_get_option( 'link_color' );
	$link_color_hvr = linetime_get_option( 'link_color_hvr' );
	$text_color = linetime_get_option( 'text_color' );

	/* SECTION BACKGROUNDS */
	$body_background = linetime_get_option( 'body_background' );

	/* PRELOADER COLORS */
	$preloader_bg_color = linetime_get_option( 'preloader_bg_color' );
	$preloader_font_color = linetime_get_option( 'preloader_font_color' );

	/* COPYRIGHTS SECTION */
	$copyrights_bg_color = linetime_get_option( 'copyrights_bg_color' );	
	$copyrights_font_color = linetime_get_option( 'copyrights_font_color' );	
	$copyrights_link_font_color = linetime_get_option( 'copyrights_link_font_color' );	
	$copyrights_link_font_color_hvr = linetime_get_option( 'copyrights_link_font_color_hvr' );		


?>
/* NAVIGATION */
.nav.navbar-nav li a{
	font-family: "<?php echo str_replace( '+', " ", $navigation_font ) ?>", sans-serif;
	font-size: <?php echo $navigation_font_size ?>;
}

.nav.navbar-nav > li li{
	border-color: <?php echo $subnavigation_border_color; ?>;
}

.nav.navbar-nav li a{
	color: <?php echo $navigation_font_color ?>;
}

.navigation-bar{
	background: <?php echo $navigation_bg_color ?>;
}

.navbar-toggle,
#navigation .nav.navbar-nav li.open > a,
#navigation .nav.navbar-nav li > a:hover,
#navigation .nav.navbar-nav li > a:focus ,
#navigation .nav.navbar-nav li > a:active,
#navigation .nav.navbar-nav li.current > a,
#navigation .navbar-nav li.current-menu-parent > a, 
#navigation .navbar-nav li.current-menu-ancestor > a, 
#navigation  .navbar-nav > li.current-menu-item  > a{
	color: <?php echo $navigation_active_color ?>;
	background: <?php echo $navigation_bg_color ?>;
}

.nav.navbar-nav li li a{
	color: <?php echo $subnavigation_font_color ?>;
	background: <?php echo $subnavigation_bg_color ?>;
}

#navigation .nav.navbar-nav li li a:hover,
#navigation .nav.navbar-nav li li a:active,
#navigation .nav.navbar-nav li li a:focus,
#navigation .nav.navbar-nav li.current > a,
#navigation .navbar-nav li li.current-menu-parent > a, 
#navigation .navbar-nav li li.current-menu-ancestor > a, 
#navigation .navbar-nav li li.current-menu-item  > a{
	color: <?php echo $subnavigation_active_color ?>;
	background: <?php echo $subnavigation_bg_color ?>;
}

/* BODY */
body{
	font-family: "<?php echo str_replace( '+', " ", $text_font ) ?>", sans-serif;
	font-size: <?php echo $text_font_size ?>;
	line-height: <?php echo $text_line_height ?>;
	background-color: <?php echo $body_background ?>;
	color: <?php echo $text_color ?>
}

a, a:visited{
	color: <?php echo $link_color ?>;
}

a:hover, a:focus, a:active, .blog-title:hover h2{
	color: <?php echo $link_color_hvr ?>;
}

h1,h2,h3,h4,h5,h6{
	font-family: "<?php echo str_replace( '+', " ", $title_font ) ?>", sans-serif;
	color: <?php echo $title_color; ?>;
}

h1{
	font-size: <?php echo $h1_font_size ?>;
	line-height: <?php echo $h1_line_height ?>;
}

h2{
	font-size: <?php echo $h2_font_size ?>;
	line-height: <?php echo $h2_line_height ?>;
}

h3{
	font-size: <?php echo $h3_font_size ?>;
	line-height: <?php echo $h3_line_height ?>;
}

h4{
	font-size: <?php echo $h4_font_size ?>;
	line-height: <?php echo $h4_line_height ?>;
}

h5{
	font-size: <?php echo $h5_font_size ?>;
	line-height: <?php echo $h5_line_height ?>;
}

h6{
	font-size: <?php echo $h6_font_size ?>;
	line-height: <?php echo $h6_line_height ?>;
}

/* PAGE TITLE */
.single-blog .blog-media
	background-color: <?php echo $page_title_bg_color ?>;
}

.single-blog .blog-media h1,
.single-blog .blog-media a, .single-blog .blog-media .post-meta{	
	color: <?php echo $page_title_font_color ?>;
}

/* SITE BUTTONS */ 
.tagcloud a, .btn, a.btn,
.form-submit #submit,
.single-blog .post-share a,
a.read-more{
	background: <?php echo $buttons_bg_color ?>;
	border-color: <?php echo $buttons_border_color ?>;
	color: <?php echo $buttons_font_color ?>;
}

.share, .share:visited{
	color: <?php echo $buttons_font_color ?>;
}

.tagcloud a:hover, .tagcloud a:focus, .tagcloud a:active,
.btn:hover, .btn:focus, .btn:active,
.form-submit #submit:hover, .form-submit #submit:focus, .form-submit #submit:active,
.pagination a.active, .pagination a:hover, .pagination a:focus, .pagination a:active,
a.read-more:hover,
.post-tags a:hover{
	background: <?php echo $buttons_bg_color_hvr ?>;
	border-color: <?php echo $buttons_border_color_hvr ?>;
	color: <?php echo $buttons_font_color_hvr ?>;
}

.read-more i{
	color: <?php echo $buttons_font_color_hvr ?>;
}

/* PRELOADER COLORS */
.preloader{
	background: <?php echo $preloader_bg_color ?>;
}

.ball-triangle-path > div{
	border-color: <?php echo $preloader_font_color ?>;
}

/* COPYRIGHTS SECTION */
.copyright, .instagram-feed{
	background: <?php echo $copyrights_bg_color ?>;
	color: <?php echo $copyrights_font_color ?>;
}

.copyright a{
	color: <?php echo $copyrights_link_font_color ?>;
}

.copyright a:hover{
	color: <?php echo $copyrights_link_font_color_hvr ?>;
}