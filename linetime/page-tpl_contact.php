<?php
/*
	Template Name: Page Contact
*/
get_header();
the_post();
?>
<section class="single-blog">
	<div class="container">	
		<div class="blog-item-content margin-bottom">

			<h2 class="post-title page"><?php the_title() ?></h2>

			<div class="post-content clearfix">
				<?php the_content(); ?>
			</div>

			<form id="comment-form" class="comment-form contact-form">
				<div class="form-group has-feedback">
					<input type="text" class="form-control name" id="name" name="name" placeholder="<?php esc_attr_e( 'Your name', 'linetime' ) ?>" />
				</div>
				<div class="form-group has-feedback">
					<input type="text" class="form-control email" id="email" name="email" placeholder="<?php esc_attr_e( 'Your email', 'linetime' ) ?>" />
				</div>
				<div class="form-group has-feedback">
					<textarea rows="10" cols="100" class="form-control message" id="message" name="message" placeholder="<?php esc_attr_e( 'Your message', 'linetime' ) ?>"></textarea>															
				</div>
				<p class="form-submit">
					<a href="javascript:;" class="send-contact btn"><?php _e( 'Send Message', 'linetime' ) ?> </a>
				</p>
				<div class="send_result"></div>
			</form>	
		</div>
	</div>
</section>
<?php get_footer(); ?>