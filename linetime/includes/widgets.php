<?php

class Custom_Widget_Posts extends WP_Widget {

	function __construct() {
		parent::__construct('custom_posts', __('Linetime Recent Posts','linetime'), array('description' =>__('Display recent posts','linetime') ));
	}

	function widget($args, $instance) {
		extract($args);
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '' ) );
		
		$title = esc_attr( $instance['title'] );
		$text = esc_attr( $instance['text'] );		

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Posts', 'linetime' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;

		if ( ! $number ){
			$number = 5;
		}
		
		?>
		<?php echo $before_widget; ?>
		<?php 
		if ( $title ){
			echo $before_title . $title . $after_title; 
		}
		?>
		
		<?php
		$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($r->have_posts()):
		?>
			<ul class="list-unstyled">
			<?php 
			while ( $r->have_posts() ) : 
				$r->the_post();
				include( locate_template( 'includes/widget-loop.php' ) );
			endwhile; ?>
			</ul>
		<?php
		endif;
		?>
		
		<?php echo $after_widget; wp_reset_postdata(); ?>
<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];

		return $instance;
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'linetime' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of posts to show:', 'linetime' ); ?></label>
		<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
<?php
	}
}


class Custom_Widget_Recent_Comments extends WP_Widget {

	function __construct() {
		parent::__construct('linetime_recent_comments', __('Linetime Recent Comments','linetime'), array('description' =>__('Display recent comments','linetime') ));
	}

	function widget( $args, $instance ) {
		global $comments, $comment;
		extract( $args );
		$output = '';
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments', 'linetime' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$comments = get_comments( apply_filters( 'widget_comments_args', array(
			'number'      => $number,
			'status'      => 'approve',
			'post_status' => 'publish'
		) ) );

		$output .= $before_widget;
		if ( $title )
			$output .= $before_title . $title . $after_title;

		$output .= '<ul class="list-unstyled no-top-padding">';
		if ( $comments ) {

			foreach ( (array) $comments as $comment) {
				$comment_text = get_comment_text( $comment->comment_ID );
				if( strlen( $comment_text ) > 80 ){
					$comment_text = substr( $comment_text, 0, 80 );
					$comment_text = substr( $comment_text, 0, strripos( $comment_text, " "  ) );
					$comment_text .= "...";
				}
				$url  = linetime_get_avatar_url( get_avatar( $comment, 60 ) );
				$name = get_comment_author();
				if ( $comment->user_id ) {
					$user = get_userdata( $comment->user_id );
					if( $user->display_name ){
					 	$name = $user->display_name;
					}
				}				
				$output .=  '<li>
								<div class="widget-image-thumb">
									<img src="'.esc_url( $url ).'" class="img-responsive" width="60" height="60" alt=""/>
								</div>
								
								<div class="widget-text">
									<p>'.$name.'</p>								
									<p class="small"><i class="fa fa-clock-o"></i>'.human_time_diff( get_comment_date('U', $comment->comment_ID), current_time('timestamp') ).__(' ago', 'linetime').'</p>
								</div>
								<div class="clearfix"></div>
								<a href="' . esc_url( get_comment_link($comment->comment_ID) ) . '" class="grey">' .$comment_text. '</a>
							</li>';
			}
		}
		$output .= '</ul>';
		$output .= $after_widget;

		echo $output;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = absint( $new_instance['number'] );
		return $instance;
	}

	function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
?>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'linetime' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number of comments to show:', 'linetime' ); ?></label>
		<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" size="3" /></p>
<?php
	}
}
class Custom_Top_Authors extends WP_Widget{
	function __construct() {
		parent::__construct('widget_top_author', __('Top Author','linetime'), array('description' =>__('Adds list of top authors.','linetime') ));
	}

	function widget($args, $instance) {
		global $wpdb;
		/** This filter is documented in wp-includes/default-widgets.php */
		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$instance['count'] = apply_filters( 'widget_title', empty( $instance['count'] ) ? '5' : $instance['count'], $instance, $this->id_base );

		echo $args['before_widget'];
		
		$authors = $wpdb->get_results( "SELECT users.ID, COUNT( posts.ID ) AS post_count FROM {$wpdb->users} AS users RIGHT JOIN {$wpdb->posts} AS posts ON posts.post_author = users.ID WHERE posts.post_type='post' AND posts.post_status='publish' GROUP BY users.ID ORDER BY post_count DESC LIMIT {$instance['count']}" );
		
		if ( !empty($instance['title']) ){
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		if( !empty( $authors ) ){
			echo '<ul class="list-unstyled no-top-padding">';
			foreach( $authors as $author ){
				$url = linetime_get_avatar_url( get_avatar( $author->ID, 60 ) );
				echo    '<li class="top-authors">
							<div class="widget-image-thumb">
								<img src="'.esc_url( $url ).'" class="img-responsive" width="60" height="60" alt=""/>
							</div>
							
							<div class="widget-text">
								<a href="'.get_author_posts_url( $author->ID ).'">
									'.get_the_author_meta( 'display_name', $author->ID ).'
								</a>
								<p class="grey">'.__( 'Wrote ', 'linetime' ).' '.$author->post_count.' '.__( 'posts', 'linetime' ).'</p>
							</div>
							<div class="clearfix"></div>
						</li>';				
			}
			echo '</ul>';
		}
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( stripslashes($new_instance['title']) );
		$instance['count'] = strip_tags( stripslashes($new_instance['count']) );
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$count = isset( $instance['count'] ) ? $instance['count'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php _e('Title:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('count') ); ?>"><?php _e('Count:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('count') ); ?>" name="<?php echo esc_attr( $this->get_field_name('count') ); ?>" value="<?php echo esc_attr( $count ); ?>" />
		</p>		
		<?php
	}
}

class Custom_Social extends WP_Widget{
	function __construct() {
		parent::__construct('widget_social', __('Social Follow','linetime'), array('description' =>__('Adds list of the social icons.','linetime') ));
	}

	function widget($args, $instance) {
		/** This filter is documented in wp-includes/default-widgets.php */
		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$facebook = !empty( $instance['facebook'] ) ? '<a href="'.esc_url( $instance['facebook'] ).'" target="_blank" class="btn"><span class="fa fa-facebook"></span></a>' : '';
		$twitter = !empty( $instance['twitter'] ) ? '<a href="'.esc_url( $instance['twitter'] ).'" target="_blank" class="btn"><span class="fa fa-twitter"></span></a>' : '';
		$google = !empty( $instance['google'] ) ? '<a href="'.esc_url( $instance['google'] ).'" target="_blank" class="btn"><span class="fa fa-google"></span></a>' : '';
		$linkedin = !empty( $instance['linkedin'] ) ? '<a href="'.esc_url( $instance['linkedin'] ).'" target="_blank" class="btn"><span class="fa fa-linkedin"></span></a>' : '';
		$pinterest = !empty( $instance['pinterest'] ) ? '<a href="'.esc_url( $instance['pinterest'] ).'" target="_blank" class="btn"><span class="fa fa-pinterest"></span></a>' : '';
		$youtube = !empty( $instance['youtube'] ) ? '<a href="'.esc_url( $instance['youtube'] ).'" target="_blank" class="btn"><span class="fa fa-youtube"></span></a>' : '';
		$flickr = !empty( $instance['flickr'] ) ? '<a href="'.esc_url( $instance['flickr'] ).'" target="_blank" class="btn"><span class="fa fa-flickr"></span></a>' : '';
		$behance = !empty( $instance['behance'] ) ? '<a href="'.esc_url( $instance['behance'] ).'" target="_blank" class="btn"><span class="fa fa-behance"></span></a>' : '';

		echo $args['before_widget'];
		
		if ( !empty($instance['title']) ){
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		echo '<div class="widget-social">';
			echo $facebook.$twitter.$google.$linkedin.$pinterest.$youtube.$flickr.$behance;
		echo '</div>';
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( stripslashes($new_instance['title']) );
		$instance['facebook'] = strip_tags( stripslashes($new_instance['facebook']) );
		$instance['twitter'] = strip_tags( stripslashes($new_instance['twitter']) );
		$instance['google'] = strip_tags( stripslashes($new_instance['google']) );
		$instance['linkedin'] = strip_tags( stripslashes($new_instance['linkedin']) );
		$instance['pinterest'] = strip_tags( stripslashes($new_instance['pinterest']) );
		$instance['youtube'] = strip_tags( stripslashes($new_instance['youtube']) );
		$instance['flickr'] = strip_tags( stripslashes($new_instance['flickr']) );
		$instance['behance'] = strip_tags( stripslashes($new_instance['behance']) );
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$facebook = isset( $instance['facebook'] ) ? $instance['facebook'] : '';
		$twitter = isset( $instance['twitter'] ) ? $instance['twitter'] : '';
		$google = isset( $instance['google'] ) ? $instance['google'] : '';
		$linkedin = isset( $instance['linkedin'] ) ? $instance['linkedin'] : '';
		$pinterest = isset( $instance['pinterest'] ) ? $instance['pinterest'] : '';
		$youtube = isset( $instance['youtube'] ) ? $instance['youtube'] : '';
		$flickr = isset( $instance['flickr'] ) ? $instance['flickr'] : '';
		$behance = isset( $instance['behance'] ) ? $instance['behance'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php _e('Title:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('facebook') ); ?>"><?php _e('Facebook:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('facebook') ); ?>" name="<?php echo esc_attr( $this->get_field_name('facebook') ); ?>" value="<?php echo esc_url( $facebook ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('twitter') ); ?>"><?php _e('Twitter:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('twitter') ); ?>" name="<?php echo esc_attr( $this->get_field_name('twitter') ); ?>" value="<?php echo esc_url( $twitter ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('google') ); ?>"><?php _e('Google +:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('google') ); ?>" name="<?php echo esc_attr( $this->get_field_name('google') ); ?>" value="<?php echo esc_url( $google ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('linkedin') ); ?>"><?php _e('Linkedin:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('linkedin') ); ?>" name="<?php echo esc_attr( $this->get_field_name('linkedin') ); ?>" value="<?php echo esc_url( $linkedin ); ?>" />
		</p>			
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('youtube') ); ?>"><?php _e('YouTube:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('youtube') ); ?>" name="<?php echo esc_attr( $this->get_field_name('youtube') ); ?>" value="<?php echo esc_url( $youtube ); ?>" />
		</p>		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('pinterest') ); ?>"><?php _e('Pinterest:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('pinterest') ); ?>" name="<?php echo esc_attr( $this->get_field_name('pinterest')); ?>" value="<?php echo esc_url( $pinterest ); ?>" />
		</p>		
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('flickr') ); ?>"><?php _e('Flickr:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('flickr') ); ?>" name="<?php echo esc_attr( $this->get_field_name('flickr') ); ?>" value="<?php echo esc_url( $flickr ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('behance') ); ?>"><?php _e('Behance:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('behance') ); ?>" name="<?php echo esc_attr( $this->get_field_name('behance') ); ?>" value="<?php echo esc_url( $behance ); ?>" />
		</p>			
		<?php
	}
}

class Custom_Subscribe extends WP_Widget{
	function __construct() {
		parent::__construct('widget_subscribe', __('Subscribe','linetime'), array('description' =>__('Adds subscribe form in the sidebar.','linetime') ));
	}

	function widget($args, $instance) {
		/** This filter is documented in wp-includes/default-widgets.php */
		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo $args['before_widget'];
		
		if ( !empty($instance['title']) ){
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		echo '<div class="subscribe-form">
				<div class="linetime-form">
					<input type="text" class="form-control email" placeholder="'.esc_attr__( 'Input email here...', 'linetime' ).'">
					<a href="javascript:;" class="btn btn-default subscribe"><i class="fa fa-rss"></i></a>
				</div>
				<div class="sub_result"></div>
			  </div>';
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( stripslashes($new_instance['title']) );
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php _e('Title:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>	
		<?php
	}
}

class Shortcode_Text extends WP_Widget{
	function __construct() {
		parent::__construct('widget_shortcode', __('Linetime Shortcode Text','linetime'), array('description' =>__('Text widget which can render shortcode.','linetime') ));
	}

	function widget($args, $instance) {
		/** This filter is documented in wp-includes/default-widgets.php */
		$instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$instance['text'] = $instance['text'];

		echo $args['before_widget'];
		
		if ( !empty($instance['title']) ){
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		echo do_shortcode( $instance['text'] );
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( stripslashes($new_instance['title']) );
		$instance['text'] = $new_instance['text'];
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$text = isset( $instance['text'] ) ? $instance['text'] : '';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php _e('Title:', 'linetime') ?></label>
			<input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="shortcode-select"><?php _e('Shortcode:', 'linetime') ?></label>
			<select id="shortcode-select" name="shortcode-select" class="shortcode-add">
				<option value=""><?php _e( '-Select-', 'linetime' ) ?></option>
				<option value="accordion"><?php _e( 'Accordion', 'linetime' ) ?></option>
				<option value="alert"><?php _e( 'Alert', 'linetime' ) ?></option>
				<option value="button"><?php _e( 'Button', 'linetime' ) ?></option>
				<option value="dropcap"><?php _e( 'Dropcap', 'linetime' ) ?></option>
				<option value="bg_gallery"><?php _e( 'Gallery', 'linetime' ) ?></option>
				<option value="icon"><?php _e( 'Icon', 'linetime' ) ?></option>
				<option value="iframe"><?php _e( 'Iframe', 'linetime' ) ?></option>
				<option value="label"><?php _e( 'Label', 'linetime' ) ?></option>
				<option value="progressbar"><?php _e( 'Progress Bar', 'linetime' ) ?></option>
				<option value="tabs"><?php _e( 'Tabs', 'linetime' ) ?></option>
				<option value="toggle"><?php _e( 'Toggle', 'linetime' ) ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('text') ); ?>"><?php _e('Text:', 'linetime') ?></label>
			<textarea type="text" class="widefat shortcode-input" id="<?php echo esc_attr( $this->get_field_id('text') ); ?>" name="<?php echo esc_attr( $this->get_field_name('text') ); ?>" ><?php echo esc_textarea( $text ); ?></textarea>
		</p>
		<?php
	}
}

function custom_widgets_init() {
	if ( !is_blog_installed() ){
		return;
	}	
	/* register new ones */
	register_widget('Custom_Widget_Posts');
	register_widget('Custom_Widget_Recent_Comments');
	register_widget('Custom_Top_Authors');
	register_widget('Custom_Social');
	register_widget('Custom_Subscribe');
	register_widget('Shortcode_Text');
}

add_action('widgets_init', 'custom_widgets_init', 1);
?>