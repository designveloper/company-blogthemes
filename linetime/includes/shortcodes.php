<?php

class Shortcodes{
	
	function __construct(){
		add_action( 'init', array( $this, 'shortcode_buttons' ) );
		add_action('wp_ajax_shortcode_call', array( $this, 'shortcode_options' ) );
		add_action('wp_ajax_nopriv_shortcode_call', array( $this, 'shortcode_options' ) );
	}

	function shortcode_buttons(){
		add_filter( "mce_external_plugins", array( $this, "add_buttons" ) );
	    add_filter( 'mce_buttons', array( $this, 'register_buttons' ) );	
	}
	

	function add_buttons( $plugin_array ) {
	    $plugin_array['linetime'] = get_template_directory_uri() . '/js/shortcodes.js';
	    return $plugin_array;
	}

	function register_buttons( $buttons ) {
	    array_push( $buttons, 'linetimegrid', 'linetimeelements' ); 
	    return $buttons;
	}

	function shortcode_options(){
		$function = 'linetime_'.$_POST['shortcode'].'_params';
		echo $this->render_options( $function() );
		die();
	}


	function render_options( $fields ){
		$fields_html = '';
		foreach( $fields as $field ){
			if( !in_array( $field['type'], array( 'css_editor', 'textarea_html' ) ) ){
				$fields_html .= '<div class="shortcode-option"><label>'.$field['heading'].'</label>';
				switch ( $field['type'] ){
					case 'textfield' : 
						$fields_html .= '<input type="text" class="shortcode-field" name="'.esc_attr( $field['param_name'] ).'" value="'.esc_attr( $field['value'] ).'">';
						break;
					case 'dropdown' :
						$options = '';
						if( !empty( $field['value'] ) ){
							foreach( $field['value'] as $option_name => $option_value ){
								$options .= '<option value="'.esc_attr( $option_value ).'">'.$option_name.'</option>';
							}
						}
						$fields_html .= '<select name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field">'.$options.'</select>';
						break;
					case 'multidropdown' :
						$options = '';
						if( !empty( $field['value'] ) ){
							foreach( $field['value'] as $option_name => $option_value ){
								$options .= '<option value="'.esc_attr( $option_value ).'">'.$option_name.'</option>';
							}
						}
						$fields_html .= '<select name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field" multiple>'.$options.'</select>';
						break;
					case 'colorpicker' :
						$fields_html .= '<input type="text" name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field shortcode-colorpicker" value="'.esc_attr( $field['value'] ).'" />';
						break;
					case 'attach_image' :
						$fields_html .= '<div class="shortcode-image-holder"></div><div class="clearfix"></div>
										<a href="javascript:;" class="shortcode-add-image button">'.__( 'Add Image', 'linetime' ).'</a>
										<input type="hidden" name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field" value="'.esc_attr( $field['value'] ).'">';
						break;	
					case 'attach_images' :
						$fields_html .= '<div class="shortcode-images-holder"></div><div class="clearfix"></div>
										<a href="javascript:;" class="shortcode-add-images button">'.__( 'Add Images', 'linetime' ).'</a>
										<input type="hidden" name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field" value="'.esc_attr( $field['value'] ).'">';
						break;
					case 'textarea' :					
						$fields_html .= '<textarea name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field">'.$field['value'].'</textarea>';
						break;
					case 'textarea_raw_html' :					
						$fields_html .= '<textarea name="'.esc_attr( $field['param_name'] ).'" class="shortcode-field">'.$field['value'].'</textarea>';
						break;						
				}
				$fields_html .= '<div class="description">'.esc_attr( $field['description'] ).'</div></div>';
			}
		}


		echo $fields_html.'<a href="javascript:;" class="shortcode-save-options button">'.__( 'Insert', 'linetime' ).'</a>';
		die();

	}
}

new Shortcodes();

?>