<?php
function linetime_gap_func( $atts, $content ){
	extract( shortcode_atts( array(
		'height' => '',
	), $atts ) );

	return '<span style="height: '.esc_attr( $height ).'; display: block;"></span>';
}

add_shortcode( 'gap', 'linetime_gap_func' );

function linetime_gap_params(){
	return array(
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Gap Height","linetime"),
			"param_name" => "height",
			"value" => '',
			"description" => __("Input gap height.","linetime")
		),
	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Gap", 'linetime'),
	   "base" => "gap",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_gap_params()
	) );
}
?>