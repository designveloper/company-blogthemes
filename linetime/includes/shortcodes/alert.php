<?php
function linetime_alert_func( $atts, $content ){
	extract( shortcode_atts( array(
		'text' => '',
		'border_color' => '',
		'bg_color' => '',
		'font_color' => '',
		'icon' => '',
		'closeable' => 'no',
		'close_icon_color' => '',
		'close_icon_color_hvr' => '',
	), $atts ) );

	$rnd = linetime_random_string();

	$style_css = '
		<style>
			.'.$rnd.'.alert .close{
				color: '.$close_icon_color.';
			}
			.'.$rnd.'.alert .close:hover{
				color: '.$close_icon_color_hvr.';
			}
		</style>
	';

	return linetime_shortcode_style( $style_css ).'
	<div class="alert '.esc_attr( $rnd ).' alert-default '.( $closeable == 'yes' ? esc_attr( 'alert-dismissible' ) : '' ).'" role="alert" style=" color: '.esc_attr( $font_color ).'; border-color: '.esc_attr( $border_color ).'; background-color: '.esc_attr( $bg_color ).';">
		'.( !empty( $icon ) && $icon !== 'No Icon' ? '<i class="fa fa-'.esc_attr( $icon ).'"></i>' : '' ).'
		'.$text.'
		'.( $closeable == 'yes' ? '<button type="button" class="close" data-dismiss="alert"> <span aria-hidden="true">×</span> <span class="sr-only">'.__( 'Close', 'linetime' ).'</span> </button>' : '' ).'
	</div>';
}

add_shortcode( 'alert', 'linetime_alert_func' );

function linetime_alert_params(){
	return array(
		array(
			"type" => "textarea",
			"holder" => "div",
			"class" => "",
			"heading" => __("Text","linetime"),
			"param_name" => "text",
			"value" => '',
			"description" => __("Input alert text.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Border Color","linetime"),
			"param_name" => "border_color",
			"value" => '',
			"description" => __("Select border color for the alert box.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Background Color Color","linetime"),
			"param_name" => "bg_color",
			"value" => '',
			"description" => __("Select background color of the alert box.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Text Color","linetime"),
			"param_name" => "font_color",
			"value" => '',
			"description" => __("Select font color for the alert box text.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Icon","linetime"),
			"param_name" => "icon",
			"value" => linetime_awesome_icons_list(),
			"description" => __("Select icon.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Closeable","linetime"),
			"param_name" => "closeable",
			"value" => array(
				__( 'No', 'linetime' ) => 'no',
				__( 'Yes', 'linetime' ) => 'yes'
			),
			"description" => __("Enable or disable alert closing.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Close Icon Color","linetime"),
			"param_name" => "close_icon_color",
			"value" => '',
			"description" => __("Select color for the close icon.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Close Icon Color On Hover","linetime"),
			"param_name" => "close_icon_color_hvr",
			"value" => '',
			"description" => __("Select color for the close icon on hover.","linetime")
		),
	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Alert", 'linetime'),
	   "base" => "alert",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_alert_params()
	) );
}
?>