<?php
function linetime_toggle_func( $atts, $content ){
	extract( shortcode_atts( array(
		'title' => '',
		'toggle_content' => '',
		'state' => '',
	), $atts ) );

	$rnd = linetime_random_string();

	return '
		<div class="panel-group" id="accordion_'.esc_attr( $rnd ).'" role="tablist" aria-multiselectable="true">
		  <div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="heading_'.esc_attr( $rnd ).'">
		      <div class="panel-title">
		        <a class="'.( $state == 'in' ? '' : esc_attr( 'collapsed' ) ).'" data-toggle="collapse" data-parent="#accordion_'.esc_attr( $rnd ).'" href="#coll_'.esc_attr( $rnd ).'" aria-expanded="true" aria-controls="coll_'.esc_attr( $rnd ).'">
		        	'.$title.'
		        	<span class="title-active"></span>
		        </a>
		      </div>
		    </div>
		    <div id="coll_'.esc_attr( $rnd ).'" class="panel-collapse collapse '.esc_attr( $state ).'" role="tabpanel" aria-labelledby="heading_'.esc_attr( $rnd ).'">
		      <div class="panel-body">
		        '.apply_filters( 'the_content', $toggle_content ).'
		      </div>
		    </div>
		  </div>
		</div>';
}

add_shortcode( 'toggle', 'linetime_toggle_func' );

function linetime_toggle_params(){
	return array(
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Title","linetime"),
			"param_name" => "title",
			"value" => '',
			"description" => __("Input toggle title.","linetime")
		),
		array(
			"type" => "textarea_raw_html",
			"holder" => "div",
			"class" => "",
			"heading" => __("Content","linetime"),
			"param_name" => "toggle_content",
			"value" => '',
			"description" => __("Input toggle title.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Default State","linetime"),
			"param_name" => "state",
			"value" => array(
				__( 'Closed', 'linetime' ) => '',
				__( 'Opened', 'linetime' ) => 'in',
			),
			"description" => __("Select default toggle state.","linetime")
		),

	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Toggle", 'linetime'),
	   "base" => "toggle",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_toggle_params()
	) );
}

?>