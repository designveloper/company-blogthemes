<?php
function linetime_accordion_func( $atts, $content ){
	extract( shortcode_atts( array(
		'titles' => '',
		'contents' => ''
	), $atts ) );

	$titles = explode( "/n/", $titles );
	$contents = explode( "/n/", $contents );

	$titles_html = '';
	$contents_html = '';

	$rnd = linetime_random_string();

	$html = '';

	if( !empty( $titles ) ){
		for( $i=0; $i<sizeof( $titles ); $i++ ){
			if( !empty( $titles[$i] ) ){
				$html .= '
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading_'.esc_attr( $i ).'">
				      <div class="panel-title">
				        <a class="'.( $i !== 0 ? esc_attr( 'collapsed' ) : '' ).'" data-toggle="collapse" data-parent="#accordion_'.esc_attr( $rnd ).'" href="#coll_'.esc_attr( $i ).'_'.esc_attr( $rnd ).'" aria-expanded="true" aria-controls="coll_'.esc_attr( $i ).'_'.esc_attr( $rnd ).'">
				        	'.$titles[$i].'
				        	<span class="title-active"></span>
				        </a>
				      </div>
				    </div>
				    <div id="coll_'.esc_attr( $i ).'_'.esc_attr( $rnd ).'" class="panel-collapse collapse '.( $i == 0 ? esc_attr( 'in' ) : '' ).'" role="tabpanel" aria-labelledby="heading_'.esc_attr( $i ).'">
				      <div class="panel-body">
				        '.( !empty( $contents[$i] ) ? apply_filters( 'the_content', $contents[$i] ) : '' ).'
				      </div>
				    </div>
				  </div>
				';
			}
		}
	}

	return '
		<div class="panel-group" id="accordion_'.esc_attr( $rnd ).'" role="tablist" aria-multiselectable="true">
		'.$html.'
		</div>';
}

add_shortcode( 'accordion', 'linetime_accordion_func' );

function linetime_accordion_params(){
	return array(
		array(
			"type" => "textarea",
			"holder" => "div",
			"class" => "",
			"heading" => __("Titles","linetime"),
			"param_name" => "titles",
			"value" => '',
			"description" => __("Input accordion titles separated by /n/.","linetime")
		),
		array(
			"type" => "textarea_raw_html",
			"holder" => "div",
			"class" => "",
			"heading" => __("Contents","linetime"),
			"param_name" => "contents",
			"value" => '',
			"description" => __("Input accordion contents separated by /n/.","linetime")
		),

	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Accordion", 'linetime'),
	   "base" => "accordion",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_accordion_params()
	) );
}
?>