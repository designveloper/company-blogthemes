<?php
function linetime_dropcap_func( $atts, $content ){

	return '<span class="dropcap">'.do_shortcode( $content ).'</span>';
}

add_shortcode( 'dropcap', 'linetime_dropcap_func' );

function linetime_dropcap_params(){
	return array();
}
?>