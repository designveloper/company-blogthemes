<?php
function linetime_button_func( $atts, $content ){
	extract( shortcode_atts( array(
		'text' => '',
		'link' => '',
		'target' => '',
		'bg_color' => '',
		'bg_color_hvr' => '',
		'border_radius' => '',
		'border_color' => '',
		'border_width' => '',
		'border_color_hvr' => '',
		'icon' => '',
		'font_color' => '',
		'font_color_hvr' => '',
		'size' => 'normal',
		'align' => '',
		'btn_width' => 'normal',
		'inline' => 'no',
		'margin' => ''
	), $atts ) );

	$rnd = linetime_random_string();

	$style_css = '
	<style>
		a.'.$rnd.', a.'.$rnd.':active, a.'.$rnd.':visited, a.'.$rnd.':focus{
			display: '.( $btn_width == 'normal' ? 'inline-block' : 'block' ).';
			'.( !empty( $bg_color ) ? 'background-color: '.$bg_color.';' : '' ).'
			'.( !empty( $font_color ) ? 'color: '.$font_color.';' : '' ).'
			'.( !empty( $border_radius ) ? 'border-radius: '.$border_radius.';' : '' ).'
			'.( !empty( $border_color ) ? 'border-color: '.$border_color.';' : '' ).'
			'.( !empty( $border_width ) ? 'border-width: '.$border_width.';' : '' ).'
		}
		a.'.$rnd.':hover{
			display: '.( $btn_width == 'normal' ? 'inline-block' : 'block' ).';
			'.( !empty( $bg_color_hvr ) ? 'background-color: '.$bg_color_hvr.';' : '' ).'
			'.( !empty( $font_color_hvr ) ? 'color: '.$font_color_hvr.';' : '' ).'
			'.( !empty( $border_color_hvr ) ? 'border-color: '.$border_color_hvr.';' : '' ).'
		}		
	</style>
	';

	return linetime_shortcode_style( $style_css ).'
	<div class="btn-wrap" style="margin: '.esc_attr( $margin ).'; text-align: '.esc_attr( $align ).'; '.( $inline == 'yes' ? esc_attr( 'display: inline-block;' ) : '' ).' '.( $inline == 'yes' && $align == 'right' ? esc_attr( 'float: right;' ) : '' ).'">
		<a href="'.esc_url( $link ).'" class="btn btn-default '.esc_attr( $size ).' '.esc_attr( $rnd ).' '.( $link != '#' && $link[0] == '#' ? esc_attr( 'slideTo' ) : '' ).'" target="'.esc_attr( $target ).'">
			'.( $icon != 'No Icon' && $icon != '' ? '<i class="fa fa-'.esc_attr( $icon ).' '.( empty( $text ) ? 'no-margin' : '' ).'"></i>' : '' ).'
			'.$text.'
		</a>
	</div>';
}

add_shortcode( 'button', 'linetime_button_func' );

function linetime_button_params(){
	return array(
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Button Text","linetime"),
			"param_name" => "text",
			"value" => '',
			"description" => __("Input button text.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Button Link","linetime"),
			"param_name" => "link",
			"value" => '',
			"description" => __("Input button link.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Select Window","linetime"),
			"param_name" => "target",
			"value" => array(
				__( 'Same Window', 'linetime' ) => '_self',
				__( 'New Window', 'linetime' ) => '_blank',
			),
			"description" => __("Select window where to open the link.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Background Color","linetime"),
			"param_name" => "bg_color",
			"value" => '',
			"description" => __("Select button background color.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Background Color On Hover","linetime"),
			"param_name" => "bg_color_hvr",
			"value" => '',
			"description" => __("Select button background color on hover.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Button Border Radius","linetime"),
			"param_name" => "border_radius",
			"value" => '',
			"description" => __("Input button border radius. For example 5px or 5ox 9px 0px 0px or 50% or 50% 50% 20% 10%.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Border Width","linetime"),
			"param_name" => "border_width",
			"value" => '',
			"description" => __("Input border width.","linetime")
		),		
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Border Color","linetime"),
			"param_name" => "border_color",
			"value" => '',
			"description" => __("Select button border color.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Border Color On Hover","linetime"),
			"param_name" => "border_color_hvr",
			"value" => '',
			"description" => __("Select button border color on hover.","linetime")
		),		
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Select Icon","linetime"),
			"param_name" => "icon",
			"value" => linetime_awesome_icons_list(),
			"description" => __("Select an icon you want to display.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Font Color","linetime"),
			"param_name" => "font_color",
			"value" => '',
			"description" => __("Select button font color.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Font Color On Hover","linetime"),
			"param_name" => "font_color_hvr",
			"value" => '',
			"description" => __("Select button font color on hover.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Button Size","linetime"),
			"param_name" => "size",
			"value" => array(
				__( 'Normal', 'linetime' ) => '',
				__( 'Medium', 'linetime' ) => 'medium',
				__( 'Large', 'linetime' ) => 'large',
			),
			"description" => __("Select button size.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Button Align","linetime"),
			"param_name" => "align",
			"value" => array(
				__( 'Left', 'linetime' ) => 'left',
				__( 'Center', 'linetime' ) => 'center',
				__( 'Right', 'linetime' ) => 'right',
			),
			"description" => __("Select button align.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Select Button Width","linetime"),
			"param_name" => "btn_width",
			"value" => array(
				__( 'Normal', 'linetime' ) => 'normal',
				__( 'Full Width', 'linetime' ) => 'full',
			),
			"description" => __("Select button alwidthign.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Display Inline","linetime"),
			"param_name" => "inline",
			"value" => array(
				__( 'No', 'linetime' ) => 'no',
				__( 'Yes', 'linetime' ) => 'yes',
			),
			"description" => __("Display button inline.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Button Margins","linetime"),
			"param_name" => "margin",
			"value" => '',
			"description" => __("Add button margins.","linetime")
		),
	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Button", 'linetime'),
	   "base" => "button",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_button_params()
	) );
}

?>