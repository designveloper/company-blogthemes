<?php
function linetime_label_func( $atts, $content ){
	extract( shortcode_atts( array(
		'text' => '',
		'bg_color' => '',
		'font_color' => '',
	), $atts ) );

	return '<span class="label label-default" style="color: '.esc_attr( $font_color ).'; background-color: '.esc_attr( $bg_color ).'">'.$text.'</span>';
}

add_shortcode( 'label', 'linetime_label_func' );

function linetime_label_params(){
	return array(
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Text","linetime"),
			"param_name" => "text",
			"value" => '',
			"description" => __("Input label text.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Background Color Color","linetime"),
			"param_name" => "bg_color",
			"value" => '',
			"description" => __("Select background color of the label.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Text Color","linetime"),
			"param_name" => "font_color",
			"value" => '',
			"description" => __("Select font color for the label text.","linetime")
		),

	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Label", 'linetime'),
	   "base" => "label",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_label_params()
	) );
}

?>