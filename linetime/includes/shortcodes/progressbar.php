<?php
function linetime_progressbar_func( $atts, $content ){
	extract( shortcode_atts( array(
		'label' => '',
		'value' => '',
		'color' => '',
		'bgcolor' => '',
		'label_color' => '',
		'height' => '',
		'font_size' => '',
		'icon' => '',
		'border_radius' => '',
		'style' => ''
	), $atts ) );

	$rnd = linetime_random_string();

	$style_css = '
	<style>
		.'.$rnd.'{
			'.( !empty( $label_color ) ? 'color: '.$label_color.';' : '' ).'
			'.( !empty( $border_radius ) ? 'border-radius: '.$border_radius.';' : '' ).'
			'.( !empty( $height ) ? 'height: '.$height.';' : '' ).'
			'.( !empty( $bgcolor ) ? 'background-color: '.$bgcolor.';' : '' ).'
		}

		.'.$rnd.' .progress-bar{
			'.( !empty( $font_size ) ? 'font-size: '.$font_size.';' : '' ).'
			'.( !empty( $height ) ? 'line-height: '.$height.';' : '' ).'
			'.( !empty( $color ) ? 'background-color: '.$color.';' : '' ).'
		}

		.'.$rnd.' .progress-bar-value{
			'.( !empty( $color ) ? 'background-color: '.$color.';' : '' ).'
			'.( !empty( $label_color ) ? 'color: '.$label_color.';' : '' ).'
		}

		.'.$rnd.' .progress-bar-value:after{
			'.( !empty( $color ) ? 'border-color: '.$color.' transparent;' : '' ).'
		}
	</style>
	';

	return linetime_shortcode_style( $style_css ).'
	<div class="progress '.esc_attr( $rnd ).'">
	  <div class="progress-bar '.esc_attr( $style ).'" style="width: '.esc_attr( $value ).'%" role="progressbar" aria-valuenow="'.esc_attr( $value ).'" aria-valuemin="0" aria-valuemax="100">
	  		<div class="progress-bar-value">'.$value.'%</div>
	  		'.( !empty( $icon ) ? '<i class="fa fa-'.esc_attr( $icon ).'"></i>' : '' ).''.$label.'
	  </div>
	</div>';
}

add_shortcode( 'progressbar', 'linetime_progressbar_func' );

function linetime_progressbar_params(){
	return array(
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Label","linetime"),
			"param_name" => "label",
			"value" => '',
			"description" => __("Input progress bar label.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Label Font Size","linetime"),
			"param_name" => "font_size",
			"value" => '',
			"description" => __("Input label font size.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Value","linetime"),
			"param_name" => "value",
			"value" => '',
			"description" => __("Input progress bar value. Input number only unit is in percentage.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Color","linetime"),
			"param_name" => "color",
			"value" => '',
			"description" => __("Select progress bar color.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Background Color","linetime"),
			"param_name" => "bgcolor",
			"value" => '',
			"description" => __("Select progress bar background color.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Label Color","linetime"),
			"param_name" => "label_color",
			"value" => '',
			"description" => __("Select progress bar label color.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Height","linetime"),
			"param_name" => "height",
			"value" => '',
			"description" => __("Input progress bar height.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Label Icon","linetime"),
			"param_name" => "icon",
			"value" => linetime_awesome_icons_list(),
			"description" => __("Select icon for the label.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Border Radius","linetime"),
			"param_name" => "border_radius",
			"value" => '',
			"description" => __("Input progress bar border radius.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Progress Bar Style","linetime"),
			"param_name" => "style",
			"value" => array(
				__( 'Normal', 'linetime' ) => '',
				__( 'Stripes', 'linetime' ) => 'progress-bar-striped',
				__( 'Active Stripes', 'linetime' ) => 'progress-bar-striped active',
			),
			"description" => __("Select progress bar style.","linetime")
		),
	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Progress Bar", 'linetime'),
	   "base" => "progressbar",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_progressbar_params()
	) );
}

?>