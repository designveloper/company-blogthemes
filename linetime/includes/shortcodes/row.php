<?php
function linetime_row_func( $atts, $content ){

	return '<div class="row">'.do_shortcode( $content ).'</div>';
}

add_shortcode( 'row', 'linetime_row_func' );

function linetime_row_params(){
	return array();
}
?>