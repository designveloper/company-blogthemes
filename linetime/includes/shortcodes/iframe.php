<?php
function linetime_iframe_func( $atts, $content ){
	extract( shortcode_atts( array(
		'link' => '',
		'proportion' => '',
	), $atts ) );

	$random = linetime_random_string();

	return '
		<div class="embed-responsive embed-responsive-'.esc_attr( $proportion ).'">
		  <iframe class="embed-responsive-item" src="'.esc_url( $link ).'"></iframe>
		</div>';
}

add_shortcode( 'iframe', 'linetime_iframe_func' );

function linetime_iframe_params(){
	return array(
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Iframe link","linetime"),
			"param_name" => "link",
			"value" => '',
			"description" => __("Input link you want to embed.","linetime")
		),
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Iframe Proportion","linetime"),
			"param_name" => "proportion",
			"value" => array(
				__( '4 by 3', 'linetime' ) => '4by3',
				__( '16 by 9', 'linetime' ) => '16by9',
			),
			"description" => __("Select iframe proportion.","linetime")
		),

	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Iframe", 'linetime'),
	   "base" => "iframe",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_iframe_params()
	) );
}

?>