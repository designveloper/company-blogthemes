<?php
function linetime_icon_func( $atts, $content ){
	extract( shortcode_atts( array(
		'icon' => '',
		'color' => '',
		'size' => '',
	), $atts ) );

	return '<span class="fa fa-'.esc_attr( $icon ).'" style="color: '.esc_attr( $color ).'; font-size: '.esc_attr( $size ).'; margin: 0px 2px;"></span>';
}

add_shortcode( 'icon', 'linetime_icon_func' );

function linetime_icon_params(){
	return array(
		array(
			"type" => "dropdown",
			"holder" => "div",
			"class" => "",
			"heading" => __("Select Icon","linetime"),
			"param_name" => "icon",
			"value" => linetime_awesome_icons_list(),
			"description" => __("Select an icon you want to display.","linetime")
		),
		array(
			"type" => "colorpicker",
			"holder" => "div",
			"class" => "",
			"heading" => __("Icon Color","linetime"),
			"param_name" => "color",
			"value" => '',
			"description" => __("Select color of the icon.","linetime")
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Icon Size","linetime"),
			"param_name" => "size",
			"value" => '',
			"description" => __("Input size of the icon.","linetime")
		),

	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Icon", 'linetime'),
	   "base" => "icon",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_icon_params()
	) );
}

?>