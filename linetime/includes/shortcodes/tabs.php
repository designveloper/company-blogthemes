<?php
function linetime_tabs_func( $atts, $content ){
	extract( shortcode_atts( array(
		'titles' => '',
		'contents' => ''
	), $atts ) );

	$titles = explode( "/n/", $titles );
	$contents = explode( "/n/", $contents );

	$titles_html = '';
	$contents_html = '';

	$random = linetime_random_string();

	if( !empty( $titles ) ){
		for( $i=0; $i<sizeof( $titles ); $i++ ){
			$titles_html .= '<li role="presentation" class="'.( $i == 0 ? esc_attr( 'active' ) : '' ).'"><a href="#tab_'.esc_attr( $i ).'_'.esc_attr( $random ).'" role="tab" data-toggle="tab">'.$titles[$i].'</a></li>';
			$contents_html .= '<div role="tabpanel" class="tab-pane fade '.( $i == 0 ? esc_attr( 'in active' ) : '' ).'" id="tab_'.esc_attr( $i ).'_'.esc_attr( $random ).'">'.( !empty( $contents[$i] ) ? apply_filters( 'the_content', $contents[$i] ) : '' ).'</div>';
		}
	}

	return '
	<!-- Nav tabs -->
	<div class="nav-tabs-wrap text-center">
		<ul class="nav nav-tabs" role="tablist">
	  		'.$titles_html.'
		</ul>
	</div>

	<!-- Tab panes -->
	<div class="tab-content">
	  '.$contents_html.'
	</div>';
}

add_shortcode( 'tabs', 'linetime_tabs_func' );

function linetime_tabs_params(){
	return array(
		array(
			"type" => "textarea",
			"holder" => "div",
			"class" => "",
			"heading" => __("Titles","linetime"),
			"param_name" => "titles",
			"value" => '',
			"description" => __("Input tab titles separated by /n/.","linetime")
		),
		array(
			"type" => "textarea_raw_html",
			"holder" => "div",
			"class" => "",
			"heading" => __("Contents","linetime"),
			"param_name" => "contents",
			"value" => '',
			"description" => __("Input tab contents separated by /n/.","linetime")
		),

	);
}

if( function_exists( 'vc_map' ) ){
	vc_map( array(
	   "name" => __("Tabs", 'linetime'),
	   "base" => "tabs",
	   "category" => __('Content', 'linetime'),
	   "params" => linetime_tabs_params()
	) );
}

?>