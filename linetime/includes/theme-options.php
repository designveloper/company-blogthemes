<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: https://docs.reduxframework.com
     * */   

    global $linetime_opts;

    if ( ! class_exists( 'Linetime_Options' ) ) {

        class linetime_Options {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Just for demo purposes. Not needed per say.
                $this->theme = wp_get_theme();

                // Set the default arguments
                $this->setArguments();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                // If Redux is running as a plugin, this will remove the demo notice and links
                //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            // Remove the demo link and the notice of integrated demo from the redux-framework plugin
            function remove_demo() {

                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }

            public function setSections() {

                /**
                 * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
                 * */
                // Background Patterns Reader
                $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
                $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
                $sample_patterns      = array();

                if ( is_dir( $sample_patterns_path ) ) :

                    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
                        $sample_patterns = array();

                        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                                $name              = explode( '.', $sample_patterns_file );
                                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                                $sample_patterns[] = array(
                                    'alt' => $name,
                                    'img' => $sample_patterns_url . $sample_patterns_file
                                );
                            }
                        }
                    endif;
                endif;

                /**********************************************************************
                ***********************************************************************
                OVERALL
                **********************************************************************/
                $this->sections[] = array(
                    'title' => __('Overall', 'linetime') ,
                    'icon' => '',
                    'desc' => __('This is basic section where you can set up main settings for your website.', 'linetime'),
                    'fields' => array(
                        //Site Logo
                        array(
                            'id' => 'site_logo',
                            'type' => 'media',
                            'title' => __('Site Logo', 'linetime') ,
                            'desc' => __('Upload site logo', 'linetime')
                        ),
                        //Favicon
                        array(
                            'id' => 'site_favicon',
                            'type' => 'media',
                            'title' => __('Site Favicon', 'linetime') ,
                            'desc' => __('Please upload favicon here in PNG or JPG format. <small>(18px 18px maximum size recommended)</small>)', 'linetime')
                        ),
                    )
                );
                
                /**********************************************************************
                ***********************************************************************
                SEO
                **********************************************************************/
                
                $this->sections[] = array(
                    'title' => __('SEO', 'linetime') ,
                    'icon' => '',
                    'desc' => __('This is important part for search engines.', 'linetime'),
                    'fields' => array(  
                        // Keywords
                        array(
                            'id' => 'seo_keywords',
                            'type' => 'text',
                            'title' => __('Keywords', 'linetime') ,
                            'desc' => __('Type here website keywords separated by comma. <small>(eg. lorem, ipsum, adiscipit)</small>.', 'linetime')
                        ) ,
                        
                        // Description
                        array(
                            'id' => 'seo_description',
                            'type' => 'textarea',
                            'title' => __('Description', 'linetime') ,
                            'desc' => __('Type here website description.', 'linetime')
                        ) ,
                    )
                );

                /**********************************************************************
                ***********************************************************************
                SHARE
                **********************************************************************/
                
                $this->sections[] = array(
                    'title' => __('Share', 'linetime') ,
                    'icon' => '',
                    'desc' => __('Post share options.', 'linetime'),
                    'fields' => array(
                        // Enable Share
                        array(
                            'id' => 'enable_share',
                            'type' => 'select',
                            'title' => __('Enable Share', 'linetime') ,
                            'desc' => __('Enable or disable post share.', 'linetime'),
                            'options' => array(
                                'yes' => __( 'Yes', 'linetime' ),
                                'no' => __( 'No', 'linetime' ),
                            ),
                            'default' => 'yes'
                        ),
                        // Share Facebook
                        array(
                            'id' => 'facebook_share',
                            'type' => 'select',
                            'title' => __('Facebook Share', 'linetime') ,
                            'desc' => __('Enable or disable post share on Facebook.', 'linetime'),
                            'options' => array(
                                'yes' => __( 'Yes', 'linetime' ),
                                'no' => __( 'No', 'linetime' ),
                            ),
                            'default' => 'yes'
                        ),
                        // Share Twitter
                        array(
                            'id' => 'twitter_share',
                            'type' => 'select',
                            'title' => __('Twitter Share', 'linetime') ,
                            'desc' => __('Enable or disable post share on Twitter.', 'linetime'),
                            'options' => array(
                                'yes' => __( 'Yes', 'linetime' ),
                                'no' => __( 'No', 'linetime' ),
                            ),
                            'default' => 'yes'
                        ),
                        // Share Google+
                        array(
                            'id' => 'google_share',
                            'type' => 'select',
                            'title' => __('Google+ Share', 'linetime') ,
                            'desc' => __('Enable or disable post share on Google+.', 'linetime'),
                            'options' => array(
                                'yes' => __( 'Yes', 'linetime' ),
                                'no' => __( 'No', 'linetime' ),
                            ),
                            'default' => 'yes'
                        ),
                        // Share Linkedin
                        array(
                            'id' => 'linkedin_share',
                            'type' => 'select',
                            'title' => __('Linkedin Share', 'linetime') ,
                            'desc' => __('Enable or disable post share on Linkedin.', 'linetime'),
                            'options' => array(
                                'yes' => __( 'Yes', 'linetime' ),
                                'no' => __( 'No', 'linetime' ),
                            ),
                            'default' => 'yes'
                        ),
                        // Share Tumblr
                        array(
                            'id' => 'tumblr_share',
                            'type' => 'select',
                            'title' => __('Tumblr Share', 'linetime') ,
                            'desc' => __('Enable or disable post share on Tumblr.', 'linetime'),
                            'options' => array(
                                'yes' => __( 'Yes', 'linetime' ),
                                'no' => __( 'No', 'linetime' ),
                            ),
                            'default' => 'yes'
                        ),
                    )
                );  
                /**********************************************************************
                ***********************************************************************
                SUBSCRIPTION
                **********************************************************************/
                
                $this->sections[] = array(
                    'title' => __('Subscription', 'linetime') ,
                    'icon' => '',
                    'desc' => __('Set up subscription API key and list ID.', 'linetime'),
                    'fields' => array(
                        // Mail Chimp API
                        array(
                            'id' => 'mail_chimp_api',
                            'type' => 'text',
                            'title' => __('API Key', 'linetime') ,
                            'desc' => __('Type your mail chimp api key.', 'linetime')
                        ) , 
                        // Mail Chimp List ID
                        array(
                            'id' => 'mail_chimp_list_id',
                            'type' => 'text',
                            'title' => __('List ID', 'linetime') ,
                            'desc' => __('Type here ID of the list on which users will subscribe.', 'linetime')
                        ) ,
                    )
                );

                /***********************************************************************
                Appearance
                **********************************************************************/
                $this->sections[] = array(
                    'title' => __('Appearance', 'linetime') ,
                    'icon' => '',
                    'desc' => __('Set up the looks.', 'linetime'),
                    'fields' => array(                       
                        /*--------------------------NAVIGATION-------------------------*/
                        array(
                            'id' => 'navigation_bg_color',
                            'type' => 'color',
                            'title' => __('Navigation Background Color', 'linetime'),
                            'desc' => __('Select background color of the navigation bar.', 'linetime'),
                            'default' => '#ffffff',
                            'transparent' => false,
                        ),
                        array(
                            'id' => 'navigation_font_color',
                            'type' => 'color',
                            'title' => __('Navigation Font Color', 'linetime'),
                            'desc' => __('Select font color of the navigation bar.', 'linetime'),
                            'transparent' => false,
                            'default' => '#404040'
                        ),
                        array(
                            'id' => 'navigation_active_color',
                            'type' => 'color',
                            'title' => __('Navigation Font Color Hover/Active', 'linetime'),
                            'desc' => __('Select font color of the navigation bar on hover / active.', 'linetime'),
                            'transparent' => false,
                            'default' => '#aaaaaa'
                        ),          
                        array(
                            'id' => 'subnavigation_bg_color',
                            'type' => 'color',
                            'title' => __('Subnavigation Background Color', 'linetime'),
                            'desc' => __('Select background color of the subnavigation bar.', 'linetime'),
                            'transparent' => false,
                            'default' => '#ffffff'
                        ),
                        array(
                            'id' => 'subnavigation_font_color',
                            'type' => 'color',
                            'title' => __('Subnavigation Font Color', 'linetime'),
                            'desc' => __('Select font color of the subnavigation bar.', 'linetime'),
                            'transparent' => false,
                            'default' => '#404040'
                        ),
                        array(
                            'id' => 'subnavigation_active_color',
                            'type' => 'color',
                            'title' => __('Subnavigation Font Color Hover/Active', 'linetime'),
                            'desc' => __('Select font color of the subnavigation bar on hover / active.', 'linetime'),
                            'transparent' => false,
                            'default' => '#aaaaaa'
                        ),      
                        array(
                            'id' => 'subnavigation_border_color',
                            'type' => 'color',
                            'title' => __('Subnavigation Border Color', 'linetime'),
                            'desc' => __('Select border color of the subnavigation bar.', 'linetime'),
                            'transparent' => false,
                            'default' => '#eeeeee'
                        ),
                        array(
                            'id' => 'navigation_font',
                            'type' => 'select',
                            'title' => __('Navigation Font', 'linetime'),
                            'desc' => __('Select navigation font.', 'linetime'),
                            'transparent' => false,
                            'options' => linetime_all_google_fonts(),
                            'default' => 'Lato'
                        ),
                        array(
                            'id' => 'navigation_font_size',
                            'type' => 'text',
                            'title' => __('Navigation Font Size', 'linetime'),
                            'desc' => __('Input navigation font size.', 'linetime'),
                            'default' => '11px'
                        ),                     
                        /*-------------------------TEXT FONT----------------------------*/
                        //Text font
                        array(
                            'id' => 'text_font',
                            'type' => 'select',
                            'title' => __('Text Font', 'linetime'),
                            'desc' => __('Select font for the regular text.', 'linetime'),
                            'options' => linetime_all_google_fonts(),
                            'default' => 'Lato'
                        ),
                        array(
                            'id' => 'text_font_size',
                            'type' => 'text',
                            'title' => __('Text Font Size', 'linetime'),
                            'desc' => __('Input text font size.', 'linetime'),
                            'default' => '14px'
                        ),
                        array(
                            'id' => 'text_line_height',
                            'type' => 'text',
                            'title' => __('Text Line Height', 'linetime'),
                            'desc' => __('Input text line height.', 'linetime'),
                            'default' => '24px'
                        ),
                        /*-----------------TITLES-----------------------------*/
                        //Text font
                        array(
                            'id' => 'title_font',
                            'type' => 'select',
                            'title' => __('Title Font', 'linetime'),
                            'desc' => __('Select font for the title text.', 'linetime'),
                            'options' => linetime_all_google_fonts(),
                            'default' => 'Montserrat'
                        ),
                        array(
                            'id' => 'h1_font_size',
                            'type' => 'text',
                            'title' => __('Heading 1 Font Size', 'linetime'),
                            'desc' => __('Input heading 1 font size.', 'linetime'),
                            'default' => '38px'
                        ),
                        array(
                            'id' => 'h1_line_height',
                            'type' => 'text',
                            'title' => __('Heading 1 Line Height', 'linetime'),
                            'desc' => __('Input heading 1 line height.', 'linetime'),
                            'default' => '1.25'
                        ),
                        array(
                            'id' => 'h2_font_size',
                            'type' => 'text',
                            'title' => __('Heading 2 Font Size', 'linetime'),
                            'desc' => __('Input heading 2 font size.', 'linetime'),
                            'default' => '32px'
                        ),
                        array(
                            'id' => 'h2_line_height',
                            'type' => 'text',
                            'title' => __('Heading 2 Line Height', 'linetime'),
                            'desc' => __('Input heading 2 line height.', 'linetime'),
                            'default' => '1.25'
                        ),
                        array(
                            'id' => 'h3_font_size',
                            'type' => 'text',
                            'title' => __('Heading 3 Font Size', 'linetime'),
                            'desc' => __('Input heading 3 font size.', 'linetime'),
                            'default' => '26px'
                        ),
                        array(
                            'id' => 'h3_line_height',
                            'type' => 'text',
                            'title' => __('Heading 3 Line Height', 'linetime'),
                            'desc' => __('Input heading 3 line height.', 'linetime'),
                            'default' => '1.25'
                        ),
                        array(
                            'id' => 'h4_font_size',
                            'type' => 'text',
                            'title' => __('Heading 4 Font Size', 'linetime'),
                            'desc' => __('Input heading 4 font size.', 'linetime'),
                            'default' => '20px'
                        ),
                        array(
                            'id' => 'h4_line_height',
                            'type' => 'text',
                            'title' => __('Heading 4 Line Height', 'linetime'),
                            'desc' => __('Input heading 4 line height.', 'linetime'),
                            'default' => '1.25'
                        ),
                        array(
                            'id' => 'h5_font_size',
                            'type' => 'text',
                            'title' => __('Heading 5 Font Size', 'linetime'),
                            'desc' => __('Input heading 5 font size.', 'linetime'),
                            'default' => '14px'
                        ),
                        array(
                            'id' => 'h5_line_height',
                            'type' => 'text',
                            'title' => __('Heading 5 Line Height', 'linetime'),
                            'desc' => __('Input heading 5 line height.', 'linetime'),
                            'default' => '1.25'
                        ),
                        array(
                            'id' => 'h6_font_size',
                            'type' => 'text',
                            'title' => __('Heading 6 Font Size', 'linetime'),
                            'desc' => __('Input heading 6 font size.', 'linetime'),
                            'default' => '12px'
                        ),
                        array(
                            'id' => 'h6_line_height',
                            'type' => 'text',
                            'title' => __('Heading 6 Line Height', 'linetime'),
                            'desc' => __('Input heading 6 line height.', 'linetime'),
                            'default' => '1.25'
                        ),
                        //Title Colors
                        array(
                            'id' => 'title_color',
                            'type' => 'color',
                            'title' => __('Title Color', 'linetime'),
                            'desc' => __('Select font color for the titles.', 'linetime'),
                            'transparent' => false,
                            'default' => '#333'
                        ),
                        // Link Colors
                        array(
                            'id' => 'link_color',
                            'type' => 'color',
                            'title' => __('Link Color', 'linetime'),
                            'desc' => __('Select link color.', 'linetime'),
                            'transparent' => false,
                            'default' => '#454545'
                        ),
                        // Link Colors On Hover
                        array(
                            'id' => 'link_color_hvr',
                            'type' => 'color',
                            'title' => __('Link Color On Hover', 'linetime'),
                            'desc' => __('Select link color on hover.', 'linetime'),
                            'transparent' => false,
                            'default' => '#aaaaaa'
                        ),
                        // Text Color                        
                        array(
                            'id' => 'text_color',
                            'type' => 'color',
                            'title' => __('Text Color', 'linetime'),
                            'desc' => __('Select font color for the text.', 'linetime'),
                            'transparent' => false,
                            'default' => '#404040'
                        ),                                            
                        /* -------------------SITE BUTTONS------------------------- */
                        //Buttons BG Color
                        array(
                            'id' => 'buttons_bg_color',
                            'type' => 'color',
                            'title' => __('Buttons Background Color', 'linetime'),
                            'desc' => __('Select background color for the buttons on the site.', 'linetime'),
                            'transparent' => false,
                        ),
                        //Buttons Border Color
                        array(
                            'id' => 'buttons_border_color',
                            'type' => 'color',
                            'title' => __('Buttons Border Color', 'linetime'),
                            'desc' => __('Select border color for the buttons on the site.', 'linetime'),
                            'transparent' => false,
                            'default' => '#DA9D67'
                        ),
                        //Buttons Border Color
                        array(
                            'id' => 'buttons_font_color',
                            'type' => 'color',
                            'title' => __('Buttons Font Color', 'linetime'),
                            'desc' => __('Select font color for the buttons on the site.', 'linetime'),
                            'transparent' => false,
                            'default' => '#454545'
                        ),
                        //Buttons BG Color
                        array(
                            'id' => 'buttons_bg_color_hvr',
                            'type' => 'color',
                            'title' => __('Buttons Background Color On Hover', 'linetime'),
                            'desc' => __('Select background color for the buttons on hover on the site.', 'linetime'),
                            'transparent' => false,
                            'default' => '#DA9D67'
                        ),
                        //Buttons Border Color
                        array(
                            'id' => 'buttons_border_color_hvr',
                            'type' => 'color',
                            'title' => __('Buttons Border Color On Hover', 'linetime'),
                            'desc' => __('Select border color on hover for the buttons on the site.', 'linetime'),
                            'transparent' => false,
                            'default' => '#DA9D67'
                        ),
                        //Buttons Border Color
                        array(
                            'id' => 'buttons_font_color_hvr',
                            'type' => 'color',
                            'title' => __('Buttons Font Color On Hover', 'linetime'),
                            'desc' => __('Select font color for the buttons on the site on hover.', 'linetime'),
                            'transparent' => false,
                            'default' => '#ffffff'
                        ),
                        // Body Background Color                        
                        array(
                            'id' => 'body_background',
                            'type' => 'color',
                            'title' => __('Body Background Color', 'linetime'),
                            'desc' => __('Select background color for the body (background of the archive title and pagination).', 'linetime'),
                            'transparent' => false,
                            'default' => '#f8f8f8'
                        ),
                        // Preloader Background Color
                        array(
                            'id' => 'preloader_bg_color',
                            'type' => 'color',
                            'title' => __('Preloader Background Color', 'linetime'),
                            'desc' => __('Select background color for the preloader.', 'linetime'),
                            'transparent' => false,
                            'default' => '#ffffff'
                        ),
                        // Preloader Icon Color
                        array(
                            'id' => 'preloader_font_color',
                            'type' => 'color',
                            'title' => __('Preloader Icon Color', 'linetime'),
                            'desc' => __('Select preloader icon color.', 'linetime'),
                            'transparent' => false,
                            'default' => '#404040'
                        ),
                        //Copyrights Background Color
                        array(
                            'id' => 'copyrights_bg_color',
                            'type' => 'color',
                            'title' => __('Copyrights Background Color', 'linetime'),
                            'desc' => __('Select background color for the copyrights section.', 'linetime'),
                            'transparent' => false,
                            'default' => '#ffffff'
                        ),
                        //Copyrights Font Color
                        array(
                            'id' => 'copyrights_font_color',
                            'type' => 'color',
                            'title' => __('Copyrights Font Color', 'linetime'),
                            'desc' => __('Select font color for the copyrights section.', 'linetime'),
                            'transparent' => false,
                            'default' => '#404040'
                        ), 
                        //Copyrights Font Color
                        array(
                            'id' => 'copyrights_link_font_color',
                            'type' => 'color',
                            'title' => __('Copyrights Link Font Color', 'linetime'),
                            'desc' => __('Select font color for the links in copyrights section.', 'linetime'),
                            'transparent' => false,
                            'default' => '#454545'
                        ),
                        //Copyrights Font Color
                        array(
                            'id' => 'copyrights_link_font_color_hvr',
                            'type' => 'color',
                            'title' => __('Copyrights Link Font Color On Hover', 'linetime'),
                            'desc' => __('Select font color for the links on hover in copyrights section.', 'linetime'),
                            'transparent' => false,
                            'default' => '#aaaaaa'
                        ),                          
                    )
                );  

                /**********************************************************************
                ***********************************************************************
                CONTACT PAGE SETTINGS
                **********************************************************************/
                
                $this->sections[] = array(
                    'title' => __('Contact Page', 'linetime') ,
                    'icon' => '',
                    'desc' => __('Contact page settings.', 'linetime'),
                    'fields' => array(
                        array(
                            'id' => 'contact_form_email',
                            'type' => 'text',
                            'title' => __('Contact Email', 'linetime') ,
                            'desc' => __('Input email where the messages should arive.', 'linetime'),
                        ),
                        array(
                            'id' => 'contact_form_subject',
                            'type' => 'text',
                            'title' => __('Contact Subject', 'linetime') ,
                            'desc' => __('Input subject for the contact email messages.', 'linetime'),
                        ),                        
                    )
                );

                /**********************************************************************
                ***********************************************************************
                INSTAGRAM API
                **********************************************************************/
                
                $this->sections[] = array(
                    'title' => __('Instagram', 'linetime') ,
                    'icon' => '',
                    'desc' => __('Setup instagram account.', 'linetime'),
                    'fields' => array(
                        array(
                            'id' => 'instagram_username',
                            'type' => 'text',
                            'title' => __('Instagram Username', 'linetime') ,
                            'desc' => __('Input instagram username from which the photos will be pulled.', 'linetime'),
                        ),
                        array(
                            'id' => 'instagram_photo_number',
                            'type' => 'text',
                            'title' => __('Number Of Photos', 'linetime') ,
                            'desc' => __('Input number of photos you wish to display.', 'linetime'),
                        ),                        
                    )
                );


                /**********************************************************************
                ***********************************************************************
                COPYRIGHTS
                **********************************************************************/
                
                $this->sections[] = array(
                    'title' => __('Copyrights', 'linetime') ,
                    'icon' => '',
                    'desc' => __('Setup copyrights section.', 'linetime'),
                    'fields' => array( 
                        //Copyrights
                        array(
                            'id' => 'copyrights',
                            'type' => 'text',
                            'title' => __('Copyrights Text', 'linetime') ,
                            'desc' => __('Input copyrights text.', 'linetime'),
                        ),
                        //Side Facebook
                        array(
                            'id' => 'copyrights_facebook',
                            'type' => 'text',
                            'title' => __('Facebook Link', 'linetime') ,
                            'desc' => __('Input facebook link.', 'linetime'),
                        ),
                        //Twitter
                        array(
                            'id' => 'copyrights_twitter',
                            'type' => 'text',
                            'title' => __('Twitter Link', 'linetime') ,
                            'desc' => __('Input twitter link.', 'linetime'),
                        ),                        
                        //Google
                        array(
                            'id' => 'copyrights_google',
                            'type' => 'text',
                            'title' => __('Google+ Link', 'linetime') ,
                            'desc' => __('Input google+ link.', 'linetime'),
                        ),
                        //Tumblr
                        array(
                            'id' => 'copyrights_tumblr',
                            'type' => 'text',
                            'title' => __('Tumblr Link', 'linetime') ,
                            'desc' => __('Input tumblr link.', 'linetime'),
                        ),
                        //Linkedin
                        array(
                            'id' => 'copyrights_linkedin',
                            'type' => 'text',
                            'title' => __('Linkedin Link', 'linetime') ,
                            'desc' => __('Input linkedin link.', 'linetime'),
                        ),
                        //Instagram
                        array(
                            'id' => 'copyrights_instagram',
                            'type' => 'text',
                            'title' => __('Instagram Link', 'linetime') ,
                            'desc' => __('Input instagram link.', 'linetime'),
                        ),
                    )
                );                 

            }

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'             => 'linetime_options',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'         => $theme->get( 'Name' ),
                    // Name that appears at the top of your panel
                    'display_version'      => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'            => 'menu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'       => true,
                    // Show the sections below the admin menu item or not
                    'menu_title'           => __( 'Linetime WP', 'redux-framework-demo' ),
                    'page_title'           => __( 'Linetime WP', 'redux-framework-demo' ),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'       => '',
                    // Set it you want google fonts to update weekly. A google_api_key value is required.
                    'google_update_weekly' => false,
                    // Must be defined to add google fonts to the typography module
                    'async_typography'     => true,
                    // Use a asynchronous font on the front end or font string
                    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                    'admin_bar'            => true,
                    // Show the panel pages on the admin bar
                    'admin_bar_icon'     => 'dashicons-portfolio',
                    // Choose an icon for the admin bar menu
                    'admin_bar_priority' => 50,
                    // Choose an priority for the admin bar menu
                    'global_variable'      => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'             => false,
                    // Show the time the page took to load, etc
                    'update_notice'        => true,
                    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                    'customizer'           => true,
                    // Enable basic customizer support
                    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                    // OPTIONAL -> Give you extra features
                    'page_priority'        => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'          => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'     => 'manage_options',
                    // Specify a custom URL to an icon
                    'last_tab'             => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'            => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'            => '_options',
                    // Page slug used to denote the panel
                    'save_defaults'        => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'         => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'         => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export'   => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'       => 60 * MINUTE_IN_SECONDS,
                    'output'               => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'           => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'             => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                    'system_info'          => false,
                    // REMOVE

                    // HINTS
                    'hints'                => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );


            }

        }

        global $linetime_opts;
        $linetime_opts = new linetime_Options();
        } else {
        echo "The class named linetime_Options has already been called. <strong>Developers, you need to prefix this class with your company name or you'll run into problems!</strong>";
    }