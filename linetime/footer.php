<?php
$instagram_username = linetime_get_option( 'instagram_username' );
if( !empty( $instagram_username ) ){
	?>
	<section class="instagram-feed clearfix">
		<p><?php _e( 'Follow Us On Instagram', 'linetime' ) ?></p>
	</section>
	<?php
}
?>

<?php
$copyrights = linetime_get_option( 'copyrights' );
$copyrights_facebook = linetime_get_option( 'copyrights_facebook' );
$copyrights_twitter = linetime_get_option( 'copyrights_twitter' );
$copyrights_google = linetime_get_option( 'copyrights_google' );
$copyrights_tumblr = linetime_get_option( 'copyrights_tumblr' );
$copyrights_linkedin = linetime_get_option( 'copyrights_linkedin' );
$copyrights_instagram = linetime_get_option( 'copyrights_instagram' );
if( !empty( $copyrights ) || !empty( $copyrights_facebook ) || !empty( $copyrights_twitter ) || !empty( $copyrights_google ) || !empty( $copyrights_tumblr ) || !empty( $copyrights_linkedin ) || !empty( $copyrights_instagram ) ):
?>
	<section class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<?php 
						$allowed_html = array(
						    'a' => array(
						        'href' => array(),
						        'title' => array()
						    ),
						    'br' => array(),
						    'em' => array(),
						    'strong' => array(),
						);					
						echo wp_kses( $copyrights, $allowed_html ); 
					?>
				</div>
				<div class="col-sm-6">
					<div class="pull-right">
						<ul class="list-inline">

							<?php if( !empty( $copyrights_facebook ) ): ?>
							<li>
								<a href="<?php echo esc_url( $copyrights_facebook ) ?>">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<?php endif; ?>

							<?php if( !empty( $copyrights_twitter ) ): ?>
							<li>
								<a href="<?php echo esc_url( $copyrights_twitter ) ?>">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<?php endif; ?>

							<?php if( !empty( $copyrights_google ) ): ?>
							<li>
								<a href="<?php echo esc_url( $copyrights_google ) ?>">
									<i class="fa fa-google-plus"></i>
								</a>
							</li>
							<?php endif; ?>

							<?php if( !empty( $copyrights_tumblr ) ): ?>
							<li>
								<a href="<?php echo esc_url( $copyrights_tumblr ) ?>">
									<i class="fa fa-tumblr"></i>
								</a>
							</li>
							<?php endif; ?>

							<?php if( !empty( $copyrights_linkedin ) ): ?>
							<li>
								<a href="<?php echo esc_url( $copyrights_linkedin ) ?>">
									<i class="fa fa-linkedin"></i>
								</a>
							</li>
							<?php endif; ?>

							<?php if( !empty( $copyrights_instagram ) ): ?>
							<li>
								<a href="<?php echo esc_url( $copyrights_instagram ) ?>">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<?php endif; ?>					
						</ul>
					</div>
				</div>				
			</div>
		</div>
	</section>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>