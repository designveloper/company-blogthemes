<?php

	/**********************************************************************
	***********************************************************************
	linetime FUNCTIONS
	**********************************************************************/


require_once( locate_template( 'includes/class-tgm-plugin-activation.php' ) );
require_once( locate_template( 'includes/widgets.php' ) );
require_once( locate_template( 'includes/fonts.php' ) );
require_once( locate_template( 'includes/shortcodes.php' ) );
require_once( locate_template( 'includes/font-icons.php' ) );
require_once( locate_template( 'includes/gallery.php' ) );
require_once( locate_template( 'includes/theme-options.php' ) );

foreach ( glob( dirname(__FILE__).DIRECTORY_SEPARATOR."includes".DIRECTORY_SEPARATOR ."shortcodes".DIRECTORY_SEPARATOR ."*.php" ) as $filename ){
	require_once $filename;
}



add_action( 'tgmpa_register', 'linetime_requred_plugins' );

function linetime_requred_plugins(){
	$plugins = array(
		array(
				'name'                 => 'Redux Options',
				'slug'                 => 'redux-framework',
				'source'               => get_stylesheet_directory() . '/lib/plugins/redux-framework.zip',
				'required'             => true,
				'version'              => '',
				'force_activation'     => false,
				'force_deactivation'   => false,
				'external_url'         => '',
		),
		array(
				'name'                 => 'User Avatar',
				'slug'                 => 'wp-user-avatar',
				'source'               => get_stylesheet_directory() . '/lib/plugins/wp-user-avatar.zip',
				'required'             => true,
				'version'              => '',
				'force_activation'     => false,
				'force_deactivation'   => false,
				'external_url'         => '',
		),
		array(
				'name'                 => 'SMeta',
				'slug'                 => 'smeta',
				'source'               => get_stylesheet_directory() . '/lib/plugins/smeta.zip',
				'required'             => true,
				'version'              => '',
				'force_activation'     => false,
				'force_deactivation'   => false,
				'external_url'         => '',
		),		
	);

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
			'domain'           => 'linetime',
			'default_path'     => '',
			'parent_menu_slug' => 'themes.php',
			'parent_url_slug'  => 'themes.php',
			'menu'             => 'install-required-plugins',
			'has_notices'      => true,
			'is_automatic'     => false,
			'message'          => '',
			'strings'          => array(
				'page_title'                      => __( 'Install Required Plugins', 'linetime' ),
				'menu_title'                      => __( 'Install Plugins', 'linetime' ),
				'installing'                      => __( 'Installing Plugin: %s', 'linetime' ),
				'oops'                            => __( 'Something went wrong with the plugin API.', 'linetime' ),
				'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ),
				'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ),
				'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ),
				'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ),
				'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ),
				'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ),
				'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ),
				'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ),
				'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
				'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
				'return'                          => __( 'Return to Required Plugins Installer', 'linetime' ),
				'plugin_activated'                => __( 'Plugin activated successfully.', 'linetime' ),
				'complete'                        => __( 'All plugins installed and activated successfully. %s', 'linetime' ),
				'nag_type'                        => 'updated'
			)
	);

	tgmpa( $plugins, $config );
}

if (!isset($content_width))
	{
	$content_width = 1920;
	}

/* do shortcodes in the excerpt */
add_filter('the_excerpt', 'do_shortcode');
	
/* include custom made widgets */
function linetime_widgets_init(){

	register_sidebar(array(
		'name' => __('Blog Sidebar', 'linetime') ,
		'id' => 'sidebar-blog',
		'before_widget' => '<div class="widget blog-item-content clearfix %2$s" >',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title-wrap"><p class="widget-title">',
		'after_title' => '</p></div>',
		'description' => __('Appears on the right side of the blog post.', 'linetime')
	));		

	register_sidebar(array(
		'name' => __('Left Sidebar', 'linetime') ,
		'id' => 'sidebar-left',
		'before_widget' => '<div class="widget blog-item-content clearfix %2$s" >',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title-wrap"><p class="widget-title">',
		'after_title' => '</p></div>',
		'description' => __('Appears on the left side of the page.', 'linetime')
	));

	register_sidebar(array(
		'name' => __('Right Sidebar', 'linetime') ,
		'id' => 'sidebar-right',
		'before_widget' => '<div class="widget blog-item-content clearfix %2$s" >',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title-wrap"><p class="widget-title">',
		'after_title' => '</p></div>',
		'description' => __('Appears on the right side of the page.', 'linetime')
	));	
}

add_action('widgets_init', 'linetime_widgets_init');

/* total_defaults */
function linetime_defaults( $id ){	
	$defaults = array(
		'site_logo' => array( 'url' => '' ),
		'site_favicon' => array( 'url' => '' ),
		'seo_keywords' => '',
		'seo_description' => '',
		'enable_share' => 'yes',
		'facebook_share' => 'yes',
		'twitter_share' => 'yes',
		'google_share' => 'yes',
		'linkedin_share' => 'yes',
		'tumblr_share' => 'yes',
		'mail_chimp_api' => '',
		'mail_chimp_list_id' => '',
		'navigation_bg_color' => '#ffffff',
		'navigation_font_color' => '#404040',
		'navigation_active_color' => '#aaaaaa',
		'subnavigation_bg_color' => '#ffffff',
		'subnavigation_font_color' => '#404040',
		'subnavigation_active_color' => '#aaaaaa',
		'subnavigation_border_color' => '#eeeeee',
		'navigation_font' => 'Lato',
		'navigation_font_size' => '11px',
		'text_font' => 'Lato',
		'text_font_size' => '14px',
		'text_line_height' => '24px',
		'title_font' => 'Montserrat',
		'h1_font_size'=>'38px',
		'h1_line_height'=>'1.25',
		'h2_font_size'=>'32px',
		'h2_line_height'=>'1.25',
		'h3_font_size'=>'26px',
		'h3_line_height'=>'1.25',
		'h4_font_size'=>'20px',
		'h4_line_height'=>'1.25',
		'h5_font_size'=>'14px',
		'h5_line_height'=>'1.25',
		'h6_font_size'=>'12px',
		'h6_line_height'=>'1.25',
		'buttons_bg_color' => '',
		'buttons_border_color' => '#DA9D67',
		'buttons_font_color' => '#454545',
		'buttons_bg_color_hvr' => '#DA9D67',
		'buttons_border_color_hvr' => '#DA9D67',
		'buttons_font_color_hvr' => '#ffffff',
		'title_color' => '#333',
		'link_color' => '#454545',
		'link_color_hvr' => '#aaaaaa',
		'text_color' => '#404040',
		'body_background' => '#f8f8f8',
		'contact_form_email' => '',
		'contact_form_subject' => '',
		'preloader_bg_color' => '#ffffff',
		'preloader_font_color' => '#404040',
		'copyrights_bg_color' => '#ffffff',
		'copyrights_font_color' => '#404040',
		'copyrights_link_font_color' => '#454545',
		'copyrights_link_font_color_hvr' => '#aaaaaa',
		'copyrights' => '',
		'copyrights_facebook' => '',
		'copyrights_twitter' => '',
		'copyrights_google' => '',
		'copyrights_tumblr' => '',
		'copyrights_linkedin' => '',
		'copyrights_instagram' => '',
	);
	
	if( isset( $defaults[$id] ) ){
		return $defaults[$id];
	}
	else{
		
		return '';
	}
}

/* get option from theme options */
function linetime_get_option($id){
	global $linetime_options;
	if( isset( $linetime_options[$id] ) ){
		$value = $linetime_options[$id];
		if( isset( $value ) ){
			return $value;
		}
		else{
			return '';
		}
	}
	else{
		return linetime_defaults( $id );
	}
}

	/* setup neccessary theme support, add image sizes */
function linetime_setup(){
	load_theme_textdomain('linetime', get_template_directory() . '/languages');
	add_theme_support('automatic-feed-links');
	add_theme_support( "title-tag" );
	add_theme_support('html5', array(
		'comment-form',
		'comment-list'
	));
	register_nav_menu('main-navigation', __('Main Navigation', 'linetime'));
	
	add_theme_support('post-thumbnails',array( 'post', 'page' ));
	add_theme_support('post-formats',array( 'gallery', 'video' ));
	
	set_post_thumbnail_size( 800 );
	add_image_size( 'box-item', 550 );
	add_image_size( 'box-item-small', 300, 168 );

	add_theme_support('custom-header');
	add_theme_support('custom-background');
	add_editor_style();
}
add_action('after_setup_theme', 'linetime_setup');


/* setup neccessary styles and scripts */
function linetime_scripts_styles(){
	wp_enqueue_style( 'linetime-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'linetime-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'linetime-magnific', get_template_directory_uri() . '/css/magnific-popup.css' );
	wp_enqueue_style( 'linetime-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css' );

	/*load selecte fonts*/
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style('linetime-title-font', $protocol.'://fonts.googleapis.com/css?family='.linetime_get_option( 'title_font' ).':400,300,700');
	wp_enqueue_style('linetime-navigation-font', $protocol.'://fonts.googleapis.com/css?family='.linetime_get_option( 'navigation_font' ).':400,300,700');
	wp_enqueue_style('linetime-text-font', $protocol.'://fonts.googleapis.com/css?family='.linetime_get_option( 'text_font' ).':400,300,700');
	
	/* load style.css */	
	if (is_singular() && comments_open() && get_option('thread_comments')){
		wp_enqueue_script('comment-reply');
	}

	wp_enqueue_script('jquery');	
	/* bootstrap */
	wp_enqueue_script('linetime-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', false, false, true);

	/* custom */
	wp_enqueue_script('linetime-magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', false, false, true);
	wp_enqueue_script('linetime-slider', get_template_directory_uri() . '/js/responsiveslides.min.js', false, false, true);
	wp_enqueue_script('linetime-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', false, false, true);
	wp_enqueue_script('linetime-instagram', get_template_directory_uri() . '/js/instagram.js', false, false, true);

	wp_register_script('linetime-custom', get_template_directory_uri() . '/js/custom.js', false, false, true);
	wp_localize_script( 'linetime-custom', 'instagram', linetime_instagram_data() );
	wp_enqueue_script( 'linetime-custom' );	

}
add_action('wp_enqueue_scripts', 'linetime_scripts_styles', 2 );

function linetime_main_style(){
	wp_enqueue_style('linetime-style', get_stylesheet_uri() , array('dashicons'));
	ob_start();
	include( locate_template( 'css/main-color.css.php' ) );
	$custom_css = ob_get_contents();
	ob_end_clean();
	wp_add_inline_style( 'linetime-style', $custom_css );		
}
add_action( 'wp_enqueue_scripts', 'linetime_main_style', 4 );

function linetime_smeta_images( $meta_key, $post_id, $default ){
	if(class_exists('SM_Frontend')){
		global $sm;
		return $result = $sm->sm_get_meta($meta_key, $post_id);
	}
	else{		
		return $default;
	}
}

function linetime_parse_video_url( $url ){
	if( stristr( $url, 'tube' ) ){
		$temp = explode( '?v=', $url );
		$url = 'http://www.youtube.com/embed/'.$temp[1].'?rel=0';
	}
	else if( stristr( $url, 'vimeo' ) ){
		$temp = explode( '/', $url );
		$url = 'http://player.vimeo.com/video/'.$temp[sizeof($temp)-1];
	}
	return $url;
}

function linetime_instagram_data(){
	$instagram_username = linetime_get_option( 'instagram_username' );
	$instagram_photo_number = linetime_get_option( 'instagram_photo_number' );
	if( empty( $instagram_photo_number ) ){
		$instagram_photo_number = 8;
	}

	if( !empty( $instagram_username ) ){
		$username_response = wp_remote_get( 'https://api.instagram.com/v1/users/search?q=' . $instagram_username . '&client_id=972fed4ff0d5444aa21645789adb0eb0' );
		$username_response_data = json_decode( $username_response['body'], true );
		
		foreach ( $username_response_data['data'] as $data ) {
			if ( $data['username'] == $instagram_username ) {
				$instagram_data = array(
					'userID' => $data['id'],
					'count' => $instagram_photo_number
				);
			}
		}

		return $instagram_data;
	}
}

function linetime_admin_scripts_styles(){
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'wp-color-picker' );
	wp_enqueue_script( 'jquery-ui-sortable' );
	wp_enqueue_script( 'jquery-ui-dialog' );

	wp_enqueue_style( 'linetime-jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_style('linetime-shortcodes-style', get_template_directory_uri() . '/css/admin.css' );
	wp_enqueue_style('linetime-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );

	if( strpos( $_SERVER['REQUEST_URI'], 'widget' ) !== false ){
		wp_enqueue_media();
		wp_enqueue_script('linetime-shortcodes', get_template_directory_uri() . '/js/shortcodes.js', false, false, true);
	}
}
add_action('admin_enqueue_scripts', 'linetime_admin_scripts_styles');

/* add admin-ajax */
function linetime_custom_head(){
	echo '<script type="text/javascript">var ajaxurl = \'' . admin_url('admin-ajax.php') . '\';</script>';
}
add_action('wp_head', 'linetime_custom_head');

/* get data of the attached image */
function linetime_get_attachment( $attachment_id, $size ){
	$attachment = get_post( $attachment_id );
	if( !empty( $attachment ) ){
	$att_data_thumb = wp_get_attachment_image_src( $attachment_id, $size );
		return array(
			'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
			'caption' => $attachment->post_excerpt,
			'description' => $attachment->post_content,
			'href' => $attachment->guid,
			'src' => $att_data_thumb[0],
			'title' => $attachment->post_title
		);
	}
	else{
		return array(
			'alt' => '',
			'caption' => '',
			'description' => '',
			'href' => '',
			'src' => '',
			'title' => '',
		);
	}
}

class linetime_walker extends Walker_Nav_Menu {
  
	/**
	* @see Walker::start_lvl()
	* @since 3.0.0
	*
	* @param string $output Passed by reference. Used to append additional content.
	* @param int $depth Depth of page. Used for padding.
	*/
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
	}

	/**
	* @see Walker::start_el()
	* @since 3.0.0
	*
	* @param string $output Passed by reference. Used to append additional content.
	* @param object $item Menu item data object.
	* @param int $depth Depth of menu item. Used for padding.
	* @param int $current_page Menu item ID.
	* @param object $args
	*/
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		/**
		* Dividers, Headers or Disabled
		* =============================
		* Determine whether the item is a Divider, Header, Disabled or regular
		* menu item. To prevent errors we use the strcasecmp() function to so a
		* comparison that is not case sensitive. The strcasecmp() function returns
		* a 0 if the strings are equal.
		*/
		if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} 
		else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} 
		else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
		} 
		else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
			$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
		} 
		else {
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			
			if ( $args->has_children ){
				$class_names .= ' dropdown';
			}
			
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names .'>';

			$atts = array();
			$atts['title'] = ! empty( $item->title )	? $item->title	: '';
			$atts['target'] = ! empty( $item->target )	? $item->target	: '';
			$atts['rel'] = ! empty( $item->xfn )	? $item->xfn	: '';

			// If item has_children add atts to a.
			$atts['href'] = ! empty( $item->url ) ? $item->url : '';
			if ( $args->has_children ) {
				$atts['data-toggle']	= 'dropdown';
				$atts['class']	= 'dropdown-toggle';
				$atts['data-hover']	= 'dropdown';
				$atts['aria-haspopup']	= 'true';
			} 

			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}

			$item_output = $args->before;

			/*
			* Glyphicons
			* ===========
			* Since the the menu item is NOT a Divider or Header we check the see
			* if there is a value in the attr_title property. If the attr_title
			* property is NOT null we apply it as the class name for the glyphicon.
			*/
			
			$item_output .= '<a'. $attributes .'>';

			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			if( $args->has_children && 0 === $depth ){
				$item_output .= ' <i class="fa fa-angle-down"></i>';
			}
			$item_output .= '</a>';
			$item_output .= $args->after;
			
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}

	/**
	* Traverse elements to create list from elements.
	*
	* Display one element if the element doesn't have any children otherwise,
	* display the element and its children. Will only traverse up to the max
	* depth and no ignore elements under that depth.
	*
	* This method shouldn't be called directly, use the walk() method instead.
	*
	* @see Walker::start_el()
	* @since 2.5.0
	*
	* @param object $element Data object
	* @param array $children_elements List of elements to continue traversing.
	* @param int $max_depth Max depth to traverse.
	* @param int $depth Depth of current element.
	* @param array $args
	* @param string $output Passed by reference. Used to append additional content.
	* @return null Null on failure with no changes to parameters.
	*/
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
		if ( ! $element )
			return;

		$id_field = $this->db_fields['id'];

		// Display this element.
		if ( is_object( $args[0] ) ){
		   $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
		}

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	/**
	* Menu Fallback
	* =============
	* If this function is assigned to the wp_nav_menu's fallback_cb variable
	* and a manu has not been assigned to the theme location in the WordPress
	* menu manager the function with display nothing to a non-logged in user,
	* and will add a link to the WordPress menu manager if logged in as an admin.
	*
	* @param array $args passed from the wp_nav_menu function.
	*
	*/
	public static function fallback( $args ) {
		if ( current_user_can( 'manage_options' ) ) {

			extract( $args );

			$fb_output = null;

			if ( $container ) {
				$fb_output = '<' . $container;

				if ( $container_id ){
					$fb_output .= ' id="' . $container_id . '"';
				}

				if ( $container_class ){
					$fb_output .= ' class="' . $container_class . '"';
				}

				$fb_output .= '>';
			}

			$fb_output .= '<ul';

			if ( $menu_id ){
				$fb_output .= ' id="' . $menu_id . '"';
			}

			if ( $menu_class ){
				$fb_output .= ' class="' . $menu_class . '"';
			}

			$fb_output .= '>';
			$fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
			$fb_output .= '</ul>';

			if ( $container ){
				$fb_output .= '</' . $container . '>';
			}

			echo $fb_output;
		}
	}
}

/*generate random password*/
function linetime_random_string( $length = 10 ) {
	$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$random = '';
	for ($i = 0; $i < $length; $i++) {
		$random .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $random;
}

/* format wp_link_pages so it has the right css applied to it */
function linetime_link_pages( $post_pages ){
	/* format pages that are not current ones */
	$post_pages = str_replace( '<a', '<a class="btn btn-default "', $post_pages );
	$post_pages = str_replace( '</span></a>', '</a>', $post_pages );
	$post_pages = str_replace( '><span>', '>', $post_pages );
	
	/* format current page */
	$post_pages = str_replace( '<span>', '<a href="javascript:;" class="btn btn-default active">', $post_pages );
	$post_pages = str_replace( '</span>', '</a>', $post_pages );
	
	return $post_pages;
	
}
/* create tags list */
function linetime_the_tags(){
	$tags = get_the_tags();
	$list = array();
	if( !empty( $tags ) ){
		foreach( $tags as $tag ){
			$list[] = '<a href="'.esc_url( get_tag_link( $tag->term_id ) ).'">'.$tag->name.'</a>';
		}
	}
	
	return join( "", $list );
}

function linetime_cloud_sizes($args) {
	$args['smallest'] = 12;
	$args['largest'] = 12;
	$args['unit'] = 'px';
	return $args; 
}
add_filter('widget_tag_cloud_args','linetime_cloud_sizes');

function linetime_the_category(){
	$list = array();
	$categories = get_the_category();
	if( !empty( $categories ) ){
		foreach( $categories as $category ){
			$list[] = '<a href="'.esc_url( get_category_link( $category->term_id ) ).'">'.$category->name.'</a> ';
		}
	}
	
	return join( ', ', $list );
}


/* format pagination so it has correct style applied to it */
function linetime_format_pagination( $page_links ){
	$list = '';
	if( !empty( $page_links ) ){
		foreach( $page_links as $page_link ){
			$page_link = str_replace( "<span class='page-numbers current'>", '<a href="javascript:;" class="active">', $page_link );
			$page_link = str_replace( '</span>', '</a>', $page_link );
			$page_link = str_replace( array( 'class="', "class='" ), array( 'class="btn btn-default ', "class='btn btn-default " ), $page_link );
			$list .= $page_link." ";
		}
	}
	
	return $list;
}


/*======================CONTACT FUNCTIONS==============*/
function linetime_send_contact(){
	$errors = array();
	$name = esc_sql( $_POST['name'] );	
	$email = esc_sql( $_POST['email'] );
	$message = esc_sql( $_POST['message'] );
	if( empty( $name ) || empty( $email ) || empty( $message ) ){
		$response = array(
			'error' => __( 'All fields are required.', 'linetime' ),
		);
	}
	else if( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
		$response = array(
			'error' => __( 'E-mail address is not valid.', 'linetime' ),
		);	
	}
	else{
		$email_to = linetime_get_option( 'contact_form_email' );
		$subject = linetime_get_option( 'contact_form_subject' );
		$message = "
			".__( 'Name: ', 'linetime' )." {$name} \n			
			".__( 'Email: ', 'linetime' )." {$email} \n
			".__( 'Message: ', 'linetime' )."\n {$message} \n
		";
		
		$info = @wp_mail( $email_to, $subject, $message );
		if( $info ){
			$response = array(
				'success' => __( 'Your message was successfully submitted.', 'linetime' ),
			);
		}
		else{
			$response = array(
				'error' => __( 'Unexpected error while attempting to send e-mail.', 'linetime' ),
			);
		}
		
	}
	
	echo json_encode( $response );
	die();	
}
add_action('wp_ajax_contact', 'linetime_send_contact');
add_action('wp_ajax_nopriv_contact', 'linetime_send_contact');

/* =======================================================SUBSCRIPTION FUNCTIONS */
function linetime_send_subscription( $email = '' ){
	$email = !empty( $email ) ? $email : $_POST["email"];
	$response = array();	
	if( filter_var( $email, FILTER_VALIDATE_EMAIL ) ){
		require_once( locate_template( 'includes/mailchimp.php' ) );
		$chimp_api = linetime_get_option("mail_chimp_api");
		$chimp_list_id = linetime_get_option("mail_chimp_list_id");
		if( !empty( $chimp_api ) && !empty( $chimp_list_id ) ){
			$mc = new MailChimp( $chimp_api );
			$result = $mc->call('lists/subscribe', array(
				'id'                => $chimp_list_id,
				'email'             => array( 'email' => $email )
			));
			
			if( $result === false) {
				$response['error'] = __( 'There was an error contacting the API, please try again.', 'linetime' );
			}
			else if( isset($result['status']) && $result['status'] == 'error' ){
				$response['error'] = json_encode($result);
			}
			else{
				$response['success'] = __( 'You have successuffly subscribed to the newsletter.', 'linetime' );
			}
			
		}
		else{
			$response['error'] = __( 'API data are not yet set.', 'linetime' );
		}
	}
	else{
		$response['error'] = __( 'Email is empty or invalid.', 'linetime' );
	}
	
	echo json_encode( $response );
	die();
}
add_action('wp_ajax_subscribe', 'linetime_send_subscription');
add_action('wp_ajax_nopriv_subscribe', 'linetime_send_subscription');

function linetime_hex2rgb( $hex ){
	$hex = str_replace("#", "", $hex);

	$r = hexdec(substr($hex,0,2));
	$g = hexdec(substr($hex,2,2));
	$b = hexdec(substr($hex,4,2));
	return $r.", ".$g.", ".$b; 
}

function linetime_get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
	if( empty( $matches[1] ) ){
		preg_match("/src=\"(.*?)\"/i", $get_avatar, $matches);
	}
    return $matches[1];
}

function linetime_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	$add_below = ''; 
	?>
	<!-- comment -->
	<div class="comment-row <?php echo $comment->comment_parent != '0' ? esc_attr('comment-margin-left') : ''; ?> clearfix" id="comment-<?php echo esc_attr( get_comment_ID() ); ?>">
		<div class="comment-avatar">
			<?php 
			$avatar = linetime_get_avatar_url( get_avatar( $comment, 50 ) );
			if( !empty( $avatar ) ): ?>
				<img src="<?php echo esc_url( $avatar ); ?>" class="img-responsive img-circle" title="" alt="">
			<?php endif; ?>
			<div class="comment-name clearfix">
				<div class="pull-left">
					<p>
					<?php 
					$name = get_comment_author();
					if ( $comment->user_id ) {
						$user = get_userdata( $comment->user_id );
						if( $user->display_name ){
						 	$name = $user->display_name;
						}
					}	
					echo $name; ?></p>
					<p class="grey"><?php comment_time( 'F j, Y '.__('@','linetime').' H:i' ); ?> </p>
				</div>
				<?php 
				comment_reply_link( 
					array_merge( 
						$args, 
						array( 
							'reply_text' => '<i class="fa fa-share"></i> <small>'.__( 'Reply', 'linetime' ).'</small>', 
							'add_below' => $add_below, 
							'depth' => $depth, 
							'max_depth' => $args['max_depth'] 
						) 
					) 
				); ?>
			</div>			
		</div>
		<div class="comment-content-wrap">
			<?php 
			if ($comment->comment_approved != '0'){
			?>
				<?php comment_text(); ?>
			<?php 
			}
			else{ ?>
				<p><?php _e('Your comment is awaiting moderation.', 'linetime'); ?></p>				
			<?php
			}
			?>			
		</div>
	</div>
	<?php  
}

function linetime_end_comments(){
	return "";
}

function linetime_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'linetime_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'linetime_embed_html' ); // Jetpack

function linetime_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$form = '<form class="protected-post-form" action="' . site_url() . '/wp-login.php?action=postpass" method="post">
				' . __( "This post is password protected. To view it please enter your password below:", "linetime" ) . '
				<label for="' . $label . '">' . __( "Password:", "linetime" ) . ' </label><div class="linetime-form"><input name="post_password" class="form-control" id="' . $label . '" type="password" /><a class="btn btn-default submit_form"><i class="fa fa-sign-in"></i></a></div>
			</form>
	';
	return $form;
}
add_filter( 'the_password_form', 'linetime_password_form' );

function linetime_shortcode_style( $style_css ){
 return '<script>jQuery(document).ready( function($){ $("head").append( \''.str_replace( array( "\n", "\r" ), " ", $style_css).'\' ); });</script>';
}


function linetime_reading_time(){
	$word = str_word_count(strip_tags( get_the_content() ) );
	$m = floor($word / 200);
	$s = floor($word % 200 / (200 / 60));
	$est = $m.__( 'min', 'linetime' ).', '.$s.__( 'sec', 'linetime' );
	echo $est;	
}

/* check if the blog has any media */
function linetime_has_media(){
	$post_format = get_post_format();
	switch( $post_format ){
	
		case 'gallery' :
			$post_meta = get_post_custom();
			$gallery_images = linetime_smeta_images( 'gallery_images', get_the_ID(), array() );		
			if( !empty( $gallery_images ) ){
				return true;
			}
			else if( has_post_thumbnail() ){
				return true;
			}			
			else{
				return false;
			}
			break;
	
		case 'video' :
			$post_meta = get_post_custom();
			$video_url = get_post_meta( get_the_ID(), 'video', true );
			if( !empty( $video_url ) ){
				return true;
			}
			else if( has_post_thumbnail() ){
				return true;
			}
			else{
				return false;
			}
			break;
			
		default: 
			$post_meta = get_post_custom();
			$iframe_standard = get_post_meta( get_the_ID(), 'iframe_standard', true );
			if( !empty( $iframe_standard ) ){
				return true;
			}
			else if( has_post_thumbnail() ){
				return true;
			}
			else{
				return false;
			}
			break;
	}	
}

function linetime_custom_meta(){

	$post_meta_standard = array(
		array(
			'id' => 'iframe_standard',
			'name' => __( 'Input url to be embeded', 'linetime' ),
			'type' => 'text',
		),
	);
	
	$meta_boxes[] = array(
		'title' => __( 'Standard Post Information', 'linetime' ),
		'pages' => 'post',
		'fields' => $post_meta_standard,
	);	
	
	$post_meta_gallery = array(
		array(
			'id' => 'gallery_images',
			'name' => __( 'Add images for the gallery', 'linetime' ),
			'type' => 'image',
			'repeatable' => 1
		)
	);

	$meta_boxes[] = array(
		'title' => __( 'Gallery Post Information', 'linetime' ),
		'pages' => 'post',
		'fields' => $post_meta_gallery,
	);	

	$post_meta_video = array(
		array(
			'id' => 'video',
			'name' => __( 'Input video URL', 'linetime' ),
			'type' => 'text',
		),
		array(
			'id' => 'video_type',
			'name' => __( 'Select video type', 'linetime' ),
			'type' => 'select',
			'options' => array(
				'self' => __( 'Self Hosted', 'linetime' ),
				'remote' => __( 'Embed', 'linetime' ),
			)
		),
	);
	
	$meta_boxes[] = array(
		'title' => __( 'Video Post Information', 'linetime' ),
		'pages' => 'post',
		'fields' => $post_meta_video,
	);	

	$post_meta_featured = array(
		array(
			'id' => 'featured_post',
			'name' => __( 'Featured Post', 'linetime' ),
			'type' => 'select',
			'options' => array(
				'no' => __( 'No', 'linetime' ),
				'yes' => __( 'Yes', 'linetime' ),
			)
		),
	);
	
	$meta_boxes[] = array(
		'title' => __( 'Featured Post', 'linetime' ),
		'pages' => 'post',
		'fields' => $post_meta_featured,
	);

	return $meta_boxes;
}
add_filter('sm_meta_boxes', 'linetime_custom_meta');


?>