<?php
/*
	Template Name: Left Sidebar
*/
get_header();
the_post();
?>
<section class="single-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php get_sidebar( 'left' ) ?>
			</div>		
			<div class="col-md-9">

				<?php if( has_post_thumbnail() ): ?>
				<div class="blog-media" style="<?php echo esc_attr( $style ); ?>">
					<?php the_post_thumbnail() ?>
				</div>	
				<?php endif; ?>			
				
				<div class="blog-item-content margin-bottom">

					<h2 class="post-title page"><?php the_title() ?></h2>

					<div class="post-content clearfix">
						<?php the_content(); ?>
					</div>

				</div>

				<?php comments_template( '', true ) ?>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>