<?php 
$iframe_standard = get_post_meta( get_the_ID() , 'iframe_standard', true );
if( !empty( $iframe_standard ) ){
	?>
	<div class="embed-responsive embed-responsive-16by9">
		<iframe src="<?php echo esc_url( $iframe_standard ) ?>" class="embed-responsive-item"></iframe>
	</div>
	<?php
}
else{
	the_post_thumbnail( $image_size );
}
?>