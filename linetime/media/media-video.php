<?php
$post_meta = get_post_custom();
$post_id = get_the_ID();
$video = get_post_meta( $post_id, 'video', true );
$video_type = get_post_meta( $post_id, 'video_type', true );
if( !empty( $video ) ){
	?>
	<div class="embed-responsive embed-responsive-16by9">
	<?php
		if( $video_type == 'self'){
			?>
			<video class="embed-responsive-item" controls>
				<source src="<?php echo esc_url( $video ); ?>" type="video/mp4">
				<?php _e( 'Your browser does not support the video tag.', 'recipe' ) ?>;
			</video>
			<?php
		}
		else{
			?>
			<iframe src="<?php echo esc_url( linetime_parse_video_url( $video ) ); ?>" class="embed-responsive-item"></iframe>
			<?php
		}
	?>
	</div>
	<?php
}
else{
	the_post_thumbnail( $image_size );
}
?>